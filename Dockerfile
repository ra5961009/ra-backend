# php/Dockerfile
FROM php:8.1-fpm

# Install necessary PHP extensions, if any
RUN docker-php-ext-install mysqli pdo pdo_mysql

# Set the working directory
WORKDIR /var/www/html

# Copy the application code
COPY . /var/www/html

# Expose the port the container will listen on
EXPOSE 9000
