<?php
namespace Models;
require_once("config.php");
use Models\Database;

class Card {
  private $db;
  private $conn;
  private $table = 'cards';

  public function __construct() {
    $this->db = new Database();
    $this->conn = $this->db->connect();
  }

  public function create($params) {
    try {
      $sql = "INSERT INTO $this->table 
      (
        name,
        image_url,
        min_points,
        max_points,
        benefit,
        qrcode_url
      ) VALUES (?,?,?,?,?,?)";
      $stmt = $this->conn->prepare($sql);
      $stmt->bind_param("ssiiss", 
        $params->name,
        $params->image_url,
        $params->min_points,
        $params->max_points,
        $params->benefit,
        $params->qrcode_url
      );
      
      $stmt->execute();
      $result = $stmt->get_result();
      return $result;
    } catch (\Exception $e) {
      throw $e;
    }
  }

  public function edit($body, $param) {
    try {
      $sql = "UPDATE $this->table SET 
        name=?,
        image_url=?,
        min_points=?,
        max_points=?,
        benefit=?,
        qrcode_url=?
      WHERE id=?";

      $stmt = $this->conn->prepare($sql);
      $stmt->bind_param("ssiisss", 
        $body->name,
        $body->image_url,
        $body->min_points,
        $body->max_points,
        $body->benefit,
        $body->qrcode_url,
        $param
      );
      
      if ($stmt->execute()) {
        $body->id = $param;
        return $body;
      }
    } catch (\Exception $e) {
      throw $e;
    }
  }

  public function _delete($param) {
    try {
      $sql = "UPDATE $this->table SET deleted_at=now() WHERE id=?";
      $stmt = $this->conn->prepare($sql);
      $stmt->bind_param("s", $param);
      
      if ($stmt->execute()) {
        return $param;
      }
    } catch (\Exception $e) {
      throw $e;
    }
  }

  public function getAll () {
    try {
      $sql = "SELECT * FROM $this->table WHERE true and deleted_at is null";
      $stmt = $this->conn->query($sql);
  
      $rows = [];
      if ($stmt->num_rows > 0) {
        while($row = $stmt->fetch_assoc()) {
          $rows[] = $row;
        }
      }
      
      $stmt->close();
      return $rows;
    } catch (Exception $e) {
      throw $e;
    }
  }
  
  public function getById ($id) {
    $sql = "SELECT * FROM $this->table WHERE id = ?";
    $stmt = $this->conn->prepare($sql);
    $stmt->bind_param("s", $id);
    $stmt->execute();
    $result = $stmt->get_result();

    $row = null;
    if ($result->num_rows > 0) {
      $row = $result->fetch_assoc();
    }

    $stmt->close();
    return $row;
  }
}