<?php
namespace Models;
require_once("config.php");
use Models\Database;

class Voucher {
  private $db;
  private $conn;
  private $table = 'vouchers';

  public function __construct() {
    $this->db = new Database();
    $this->conn = $this->db->connect();
  }

  public function create($params) {
    try {
      $sql = "INSERT INTO $this->table 
      (
        hotels_id,
        article_number,
        amount,
        minimum_spending,
        title,
        description,
        term_condition,
        category,
        code,
        qty,
        expire_date,
        points,
        image_url,
        type,
        plu,
        discount,
        outlet_code
      ) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) RETURNING id";
      $stmt = $this->conn->prepare($sql);
      $stmt->bind_param("ssiisssssisisssis", 
        $params->hotels_id,
        $params->article_number,
        $params->amount,
        $params->minimum_spending,
        $params->title,
        $params->description,
        $params->term_condition,
        $params->category,
        $params->code,
        $params->qty,
        $params->expire_date,
        $params->points,
        $params->image_url,
        $params->type,
        $params->plu,
        $params->discount,
        $params->outlet
      );
      
      $stmt->execute();
      $result = $stmt->get_result();
      return $result;
    } catch (\Exception $e) {
      throw $e;
    }
  }

  public function edit($body, $param) {
    try {
      $sql = "UPDATE $this->table SET 
        hotels_id = ?,
        article_number = ?,
        amount = ?,
        minimum_spending = ?,
        title = ?,
        description = ?,
        term_condition = ?,
        category = ?,
        code = ?,
        qty = ?,
        expire_date = ?,
        points = ?,
        image_url = ?,
        type = ?,
        plu = ?,
        discount = ?,
        outlet_code = ?
      WHERE id=?";

      $stmt = $this->conn->prepare($sql);
      $stmt->bind_param("ssiisssssisissssis", 
        $body->hotels_id,
        $body->article_number,
        $body->amount,
        $body->minimum_spending,
        $body->title,
        $body->description,
        $body->term_condition,
        $body->category,
        $body->code,
        $body->qty,
        $body->expire_date,
        $body->points,
        $body->image_url,
        $body->type,
        $body->plu,
        $body->discount,
        $body->outlet,
        $param
      );
      
      if ($stmt->execute()) {
        $body->id = $param;
        return $body;
      }
    } catch (\Exception $e) {
      throw $e;
    }
  }

  public function updateQty($id, $qty) {
    try {
      $sql = "UPDATE $this->table SET 
        qty = ?
      WHERE id=?";

      $stmt = $this->conn->prepare($sql);
      $stmt->bind_param("is", 
        $qty,
        $id
      );
      
      if ($stmt->execute()) {
        return $id;
      }
    } catch (\Exception $e) {
      throw $e;
    }
  }

  public function _delete($param) {
    try {
      $sql = "UPDATE $this->table SET deleted_at=now() WHERE id=?";
      $stmt = $this->conn->prepare($sql);
      $stmt->bind_param("s", $param);
      
      if ($stmt->execute()) {
        return $param;
      }
    } catch (\Exception $e) {
      throw $e;
    }
  }

  public function getAll ($limit, $offset, $search, $sort) {
    try {
      $src = '';
      $order = '';
      if ($search) $src .= "and (code like '%$search%' or title like '%$search%')";
      if (sizeof($sort) == 2) $order .= "ORDER BY $sort[0] $sort[1]";

      $sql = "
        SELECT 
          *, 
          c.full_count 
        FROM 
          $this->table 
          RIGHT JOIN (SELECT count(*) AS full_count FROM $this->table WHERE true and deleted_at is null $src) c ON true
        WHERE true
          and deleted_at is null
          $src
        $order
        LIMIT ?
        OFFSET ?
      ";

      $stmt = $this->conn->prepare($sql);
      $stmt->bind_param("ii", $limit, $offset);
      $stmt->execute();
      $result = $stmt->get_result();

      $rows = [];
      $total = 0;

      if ($result->num_rows > 0) {
        while($row = $result->fetch_assoc()) {
          if($total == 0) $total = $row["full_count"];
          unset($row["full_count"]);
          $rows[] = $row;
        }
      }
  
      $stmt->close();
      return ["rows" => $rows, "total" => $total];
    } catch (\Exception $e) {
      throw $e;
    }
  }
  
  public function getById ($id) {
    $sql = "SELECT * FROM $this->table WHERE id = ?";
    $stmt = $this->conn->prepare($sql);
    $stmt->bind_param("s", $id);
    $stmt->execute();
    $result = $stmt->get_result();

    $row = null;
    if ($result->num_rows > 0) {
      $row = $result->fetch_assoc();
    }

    $stmt->close();
    return $row;
  }
}