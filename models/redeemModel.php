<?php
namespace Models;
require_once("config.php");
use Models\Database;

class Redeem {
  private $db;
  private $conn;
  private $table = 'redeem';

  public function __construct() {
    $this->db = new Database();
    $this->conn = $this->db->connect();
  }
      
  public function getAll($limit, $offset, $search, $sort) {
    try {
      $src = '';
      $order = '';
      if ($search) $src .= "and (v.title like '%$search%' or v.code like '%$search%')";
      if (sizeof($sort) == 2) $order .= "ORDER BY $sort[0] $sort[1]";

      $sql = "
        WITH redeems AS (
          SELECT 
            r.id, 
            m.id as member_id, 
            v.id as voucher_id, 
            v.code as voucher_code, 
            v.title as voucher_name, 
            v.points as voucher_points,
            v.expire_date,
            v.image_url as image_url,
            v.description as description,
            v.category as category,
            r.created_at,
            r.deleted_at,
            r.status,
            ROW_NUMBER() OVER (PARTITION BY r.voucher_id ORDER BY r.created_at DESC) AS rn
          FROM 
            redeem r 
            left join members m on m.id=r.member_id 
            left join vouchers v on v.id=r.voucher_id
          WHERE true 
            and r.deleted_at is null
            $src
        )
        SELECT 
          member_id,
          voucher_id,
          voucher_code,
          voucher_name,
          voucher_points,
          expire_date,
          image_url,
          description,
          category,
          status,
          created_at,
          c.full_count
        FROM 
          redeems
          RIGHT JOIN (SELECT count(*) AS full_count FROM redeems) c ON true
        WHERE true
          and rn = 1
          $order
        LIMIT ?
        OFFSET ?
      ";

      $stmt = $this->conn->prepare($sql);
      $stmt->bind_param("ii", $limit, $offset);
      $stmt->execute();
      $result = $stmt->get_result();

      $rows = [];
      $total = 0;

      if ($result->num_rows > 0) {
        while($row = $result->fetch_assoc()) {
          if($total == 0) $total = $row["full_count"];
          unset($row["full_count"]);
          $rows[] = $row;
        }
      }
  
      $stmt->close();
      return ["rows" => $rows, "total" => $total];
    } catch (Exception $e) {
      throw $e;
    }
  }

  public function getByMemberId($id) {
    try {
      $sql = "
        SELECT 
          r.id, 
          m.id as member_id, 
          v.id as voucher_id, 
          v.code as voucher_code, 
          v.title as voucher_name, 
          v.points as voucher_points,
          r.created_at,
          r.status
        FROM 
          $this->table r 
          left join members m on m.id=r.member_id 
          left join vouchers v on v.id=r.voucher_id
        WHERE true
          and r.deleted_at is null
          and m.id = ?
      ";

      $stmt = $this->conn->prepare($sql);
      $stmt->bind_param("s", $id);
      $stmt->execute();
      $result = $stmt->get_result();

      $rows = [];
      if ($result->num_rows > 0) {
        while($row = $result->fetch_assoc()) {
          $rows[] = $row;
        }
      }

      $stmt->close();
      return $rows;
    } catch (Exception $e) {
      throw $e;
    }
  }

  public function create($params) {
    try {
      $sql = "INSERT INTO $this->table 
      (
        member_id,
        voucher_id,
        member_code,
        voucher_code
      ) VALUES (?,?,?,?)";
      $stmt = $this->conn->prepare($sql);
      $stmt->bind_param("ssss", 
         $params->member_id, 
         $params->voucher_id,
         $params->member_code, 
         $params->voucher_code
      );
      
      $stmt->execute();
    
      // $this->updatePointMember($params);
      // $this->updateQtyVoucher($params);
      
      $result = $stmt->get_result();
      return $result;
    } catch (\Exception $e) {
        print_r($e);
    }
  }
  
  // public function updatePointMember($params) {
  //   try {
  //     $sql = "UPDATE members SET total_point_balance = total_point_balance - (SELECT points FROM vouchers WHERE id = ?) WHERE id = ?";
  //     $stmt = $this->conn->prepare($sql);
  //     $stmt->bind_param("ss", 
  //        $params->voucher_id,
  //        $params->member_id
  //     );
  //     $stmt->execute();
  //   } catch (\Exception $e) {
  //       print_r($e);
  //   }
  // }
  
  // public function updateQtyVoucher($params) {
  //   try {
  //       $sql = "UPDATE vouchers SET qty = (qty - 1) WHERE id = ?";
        
  //       $stmt = $this->conn->prepare($sql);
  //       $stmt->bind_param("s", 
  //            $params->voucher_id
  //       );
  //       $stmt->execute();
  //   } catch (\Exception $e) {
  //       print_r($e);
  //   }
  // }

  public function redeem($params) {
    try {
      $sql = "UPDATE redeem SET status = 'redeemed', updated_at=now() WHERE id = ?";
      $stmt = $this->conn->prepare($sql);
      $stmt->bind_param("s", 
        $params
      );
      $stmt->execute();
      return $params;
    } catch (\Exception $e) {
      print_r($e);
    }
  }

  public function getById($id) {
    $sql = "SELECT * FROM $this->table WHERE id = ?";
    $stmt = $this->conn->prepare($sql);
    $stmt->bind_param("s", $id);
    $stmt->execute();
    $result = $stmt->get_result();

    $row = null;
    if ($result->num_rows > 0) {
      $row = $result->fetch_assoc();
    }

    $stmt->close();
    return $row;
  }

  public function getByMemberIdAndVoucherId($memberId, $voucherId) {
    $sql = "SELECT COUNT(*) AS total FROM $this->table WHERE member_id = ? AND voucher_id = ?";
    $stmt = $this->conn->prepare($sql);
    $stmt->bind_param("ss", $memberId, $voucherId);
    $stmt->execute();
    $result = $stmt->get_result();

    $total = 0;
    if ($result->num_rows > 0) {
      $row = $result->fetch_assoc();
      $total = $row['total'];
    }

    $stmt->close();
    return $total;
  }
}