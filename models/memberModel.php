<?php
namespace Models;
require_once("config.php");
use Models\Database;

class Member {
  private $db;
  private $conn;
  private $table = 'members';

  public function __construct() {
    $this->db = new Database();
    $this->conn = $this->db->connect();
  }

  public function getAll ($limit, $offset, $search, $sort, $filters, $query) {
    try {
      $src = '';
      $order = '';
      $filter = '';
      
      if ($search) $src .= "and (first_name like '%$search%' or last_name like '%$search%' or code like '%$search%' or email like '%$search%')";
      if (sizeof($sort) > 0) {
        $order .= "ORDER BY ";
        foreach($sort as $s) {
          $ss = explode(":",$s);
          $order .= "$ss[0] $ss[1]".",";
        }

        $order = rtrim($order, ",");
      }

      if (sizeof($filters) > 0) {
        foreach($filters as $f) {
          $fa = explode(':', $f);
          $arr = explode(",", $fa[1]);
          $arr = array_map(function($str) { return str_replace('%20', ' ', $str); }, $arr);
          $column = $fa[0];
          $values = "('" . implode("','", $arr) . "') ";
          $filter .= " and $fa[0] in $values";
        }
      }

      if (isset($query['joined_start_date']) && isset($query['joined_end_date'])) {
        if ($query['joined_start_date']!= "" && $query['joined_end_date']!= "") {
          $start = $query['joined_start_date'];
          $end = $query['joined_end_date'];
          $filter .= " and created_at >= '$start' AND created_at <= '$end'";
        }
      }

      $sql = "
        SELECT 
          *, 
          (SELECT name FROM cards  WHERE total_point_balance BETWEEN min_points AND max_points LIMIT 1) as new_tier,
          c.full_count 
        FROM 
          $this->table 
          RIGHT JOIN (SELECT count(*) AS full_count FROM members WHERE true and deleted_at is null $filter $src) c ON true
        WHERE true
          and deleted_at is null
          $filter
          $src
        $order
        LIMIT ?
        OFFSET ?
      ";

      $stmt = $this->conn->prepare($sql);
      $stmt->bind_param("ii", $limit, $offset);
      $stmt->execute();
      $result = $stmt->get_result();

      $rows = [];
      $total = 0;

      if ($result->num_rows > 0) {
        while($row = $result->fetch_assoc()) {
          if($total == 0) $total = $row["full_count"];
          unset($row["full_count"]);
          $rows[] = $row;
        }
      }
  
      $stmt->close();
      return ["rows" => $rows, "total" => $total];
    } catch (Exception $e) {
      throw $e;
    }
  }

  public function updatePoint($id, $point) {
    try {
      $sql = "UPDATE $this->table SET 
        total_point_balance = ?
      WHERE id=?";

      $stmt = $this->conn->prepare($sql);
      $stmt->bind_param("is", 
        $point,
        $id
      );
      
      if ($stmt->execute()) {
        return $id;
      }
    } catch (\Exception $e) {
      throw $e;
    }
  }

  public function updateFcmTokenByEmail($username, $fcmToken) {
    try {
      $sql = "UPDATE $this->table SET 
        fcm_token = ?
      WHERE email=?";

      $stmt = $this->conn->prepare($sql);
      $stmt->bind_param("ss", 
        $fcmToken,
        $username
      );
      
      $stmt->execute();
        
    } catch (\Exception $e) {
      throw $e;
    }
  }

  public function updateActive($params) {
    try {
      $sql = "UPDATE $this->table SET is_active = CASE WHEN is_active = 1 THEN 0 ELSE 1 END WHERE id = ?";

      $stmt = $this->conn->prepare($sql);
      $stmt->bind_param("s", $params->id);
      
      $stmt->execute() ;
      
    } catch (\Exception $e) {
      throw $e;
    }
  }

  public function updateFcmTokenByPhone($username, $fcmToken) {
    try {
      $sql = "UPDATE $this->table SET 
        fcm_token = ?
      WHERE phone_number=?";

      $stmt = $this->conn->prepare($sql);
      $stmt->bind_param("ss", 
        $fcmToken,
        $username
      );
      
      $stmt->execute();
    } catch (\Exception $e) {
      throw $e;
    }
  }

  public function getById ($id) {
    $sql = "SELECT * FROM $this->table WHERE id = ? or code = ? ";
    $stmt = $this->conn->prepare($sql);
    $stmt->bind_param("ss", $id, $id);
    $stmt->execute();
    $result = $stmt->get_result();

    $row = null;
    if ($result->num_rows > 0) {
      $row = $result->fetch_assoc();
    }

    $stmt->close();
    return $row;
  }

  public function getByEmail ($email) {
    $sql = "SELECT * FROM $this->table WHERE true and deleted_at is null and email = ?";
    $stmt = $this->conn->prepare($sql);
    $stmt->bind_param("s", $email);
    $stmt->execute();
    $result = $stmt->get_result();

    $row = null;
    if ($result->num_rows > 0) {
      $row = $result->fetch_assoc();
    }

    $stmt->close();
    return $row;
  }

  public function getByPhone ($phone_number) {
    $sql = "SELECT * FROM $this->table WHERE true and deleted_at is null and phone_number = ?";
    $stmt = $this->conn->prepare($sql);
    $stmt->bind_param("s", $phone_number);
    $stmt->execute();
    $result = $stmt->get_result();

    $row = null;
    if ($result->num_rows > 0) {
      $row = $result->fetch_assoc();
    }

    $stmt->close();
    return $row;
  }

  public function checkEmailPhone ($email, $phone_number) {
    $sql = "SELECT * FROM $this->table WHERE email = ? OR phone_number = ?";
    $stmt = $this->conn->prepare($sql);
    $stmt->bind_param("ss", $email, $phone_number);
    $stmt->execute();
    $result = $stmt->get_result();

    $row = null;
    if ($result->num_rows > 0) {
      $row = $result->fetch_assoc();
    }

    $stmt->close();
    return $row;
  }

  public function create($params) {
    try {
      $code = $this->generateUniqueCode();
      $title = $params->gender == "male" ? "Mr." : "Mrs.";
      $sql = "INSERT INTO $this->table 
      (
        code,
        first_name,
        last_name,
        email,
        password,
        phone_number,
        title,
        birth_date,
        gender,
        register_date,
        tier,
        total_point_balance
      ) VALUES (?,?,?,?,?,?,?,?,?,?,?,( select value from starting_points limit 1 ))";
      $stmt = $this->conn->prepare($sql);
      $stmt->bind_param("sssssssssss", 
        $code,
        $params->first_name, 
        $params->last_name, 
        $params->email, 
        $params->password, 
        $params->phone_number,
        $title,
        $params->birth_date,
        $params->gender,
        $params->date_joined, 
        $params->tier
      );
      
      $stmt->execute();

      $this->sendActivationEmail($params, $code);

      $result = $stmt->get_result();
      return $result;
    } catch (\Exception $e) {
      throw $e;
    }
  }

  private function sendActivationEmail($params, $code) {
    // Contoh penggunaan
      $to = $params->email;
      $subject = "Aktivasi Akun Anda";
      $body = "
      Hi, ".$params->first_name." ".$params->last_name."
      <br /><br />
      Terima kasih telah melakukan pendaftaran akun, akun dapat digunakan sebagai pendaftaran Aplikasi Auro Ra
      <br /><br />
      Sebelum dapat menggunakan akun ini anda harus melakukan aktivasi akun : ".$params->email.", 
      <br /><br />
      Klik link berikut untuk mengaktivasi akun Anda: <a href='https://api.aurora.fivto.id/activate.php?lorem=lorem&email=dleader.zaii@gmail.com&name=$params->first_name&phone=$params->phone_number&tier=$params->tier&register_date=$params->date_joined&gender=$params->gender&total_point_balance=$params->total_point_balance&date_joined=$params->date_joined&tier=md5($params->tier)&code=$code&email=$params->email'>Aktivasi Akun</a>
      <br /><br />
      Selamat Mencoba, Salam Sukes !
      ";
      $this->sendEmail($to, $subject, $body);
  }

  private function sendEmail($to, $subject, $body) {
    $from = "no-reply@loyalty.fivto.id";
    $headers = "MIME-Version: 1.0" . "\r\n";
    $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
    $headers .= "From: <$from>" . "\r\n";

    if (mail($to, $subject, $body, $headers)) {
        echo "Email berhasil dikirim ke $to";
    } else {
        echo "Gagal mengirim email ke $to";
    }
  }

  private function generateUniqueCode() {
    do {
        // Membuat kode acak dengan 6 digit angka
        $code = str_pad(rand(0, 999999), 6, '0', STR_PAD_LEFT);

        // Memeriksa apakah kode sudah ada di database
        $sql = "SELECT COUNT(*) FROM $this->table WHERE code = ?";
        $stmt = $this->conn->prepare($sql);
        $stmt->bind_param("s", $code);
        $stmt->execute();
        $stmt->bind_result($count);
        $stmt->fetch();
        $stmt->close();

    } while ($count > 0); // Ulangi jika kode sudah ada

    return $code;
}

  public function createPoints($params) {
    try {
      $sql = "INSERT INTO points 
      (
        member_id,
        phone_number,
        point,
        operation,
        note
      ) VALUES (?,?,( select value from starting_points limit 1 ),?,?)";
      $stmt = $this->conn->prepare($sql);
      $stmt->bind_param("ssss", 
        $params->member_id, 
        $params->phone_number, 
        $params->operation, 
        $params->note
      );
      
      $stmt->execute();
      $result = $stmt->get_result();
      return $result;
    } catch (\Exception $e) {
      throw $e;
    }
  }

  public function edit($body, $param) {
    try {
      $sql = "UPDATE $this->table SET 
        code=?,
        email=?,
        phone_number=?,
        first_name=?,
        last_name=?,
        register_date=?,
        tier=?,
        birth_date=?,
        total_point_balance=?,
        img_url=?
      WHERE id=?";

      $stmt = $this->conn->prepare($sql);
      $stmt->bind_param("ssssssssiss", 
        $body->code, 
        $body->email, 
        $body->phone_number, 
        $body->first_name, 
        $body->last_name, 
        $body->register_date, 
        $body->tier, 
        $body->birth_date,
        $body->total_point_balance,
        $body->img_url,
        $param
      );
      
      if ($stmt->execute()) {
        $body->id = $param;
        return $body;
      }
    } catch (\Exception $e) {
      throw $e;
    }
  }

  public function _delete($param) {
    try {
      $sql = "UPDATE $this->table SET deleted_at=now() WHERE id=?";
      $stmt = $this->conn->prepare($sql);
      $stmt->bind_param("s", $param);
      
      if ($stmt->execute()) {
        return $param;
      }
    } catch (\Exception $e) {
      throw $e;
    }
  }

  public function update($params) {
    $sql = "UPDATE $this->table set first_name=?, last_name=?, email=?, phone_number=?, img_url=? where id=?";
    $stmt = $this->conn->prepare($sql);
    $stmt->bind_param("ssssss", $params->first_name, $params->last_name, $params->email, $params->phone_number, $params->img_url, $params->id);
    $stmt->execute();
    $result = $stmt->get_result();
    return $result;
  }

  public function set_starting_point($param) {
    $sql = "UPDATE starting_points set value=?";
    $stmt = $this->conn->prepare($sql);
    $stmt->bind_param("s", $param->value);
    $stmt->execute();
    $result = $stmt->get_result();
    return true;
  }

  public function get_starting_point () {
    $sql = "SELECT * FROM starting_points";
    $stmt = $this->conn->prepare($sql);
    $stmt->execute();
    $result = $stmt->get_result();

    $row = null;
    if ($result->num_rows > 0) {
      $row = $result->fetch_assoc();
    }

    $stmt->close();
    return $row;
  }
}