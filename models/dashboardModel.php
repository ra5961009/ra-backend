<?php
namespace Models;
require_once("config.php");
use Models\Database;

class Dashboard {
  private $db;
  private $conn;
  public function __construct() {
    $this->db = new Database();
    $this->conn = $this->db->connect();
  }

  public function summary() {
    try {
      $sql = "
      with 
        members_sum as (
          select 
            sum(
              case 
                when deleted_at is null then 1 else 0
              end
            ) as total_members,
            sum(
              case 
                when (DATE_FORMAT(m.register_date, '%m') = DATE_FORMAT(CURRENT_TIMESTAMP(), '%m')) then 1 else 0
              end
            ) as member_registred_last_month,
            sum(
              case 
                when (DATE_FORMAT(m.register_date, '%m') = DATE_FORMAT(CURRENT_TIMESTAMP(), '%m') - 1) then 1 else 0
              end
            ) as member_registred_prev_month
          from 
            members m 
          where 
            deleted_at is null
        ),
        vouchers_redeemed_sum as (
          select 
            sum(
              case 
                when r.status = 'redeemed' then 1 else 0
              end
            ) as total_voucher_redeemed,
            sum(
              case 
                when r.status = 'redeemed' && (DATE_FORMAT(r.updated_at, '%m') = DATE_FORMAT(CURRENT_TIMESTAMP(), '%m')) then 1 else 0
              end
            ) as redeemed_voucher_last_month,
            sum(
              case 
                when r.status = 'redeemed' && (DATE_FORMAT(r.updated_at, '%m') = DATE_FORMAT(CURRENT_TIMESTAMP(), '%m') - 1) then 1 else 0
              end
            ) as redeemed_voucher_prev_month
          from 
            vouchers v
            left join redeem r on r.voucher_id = v.id
          where 
            v.deleted_at is null
        ),
        vouchers_redeem_sum as (
          select 
            sum(
              case 
                when r.status = 'redeem' then 1 else 0
              end
            ) as total_voucher_redeem,
            sum(
              case 
                when r.status = 'redeem' && (DATE_FORMAT(r.updated_at, '%m') = DATE_FORMAT(CURRENT_TIMESTAMP(), '%m')) then 1 else 0
              end
            ) as redeem_voucher_last_month,
            sum(
              case 
                when r.status = 'redeem' && (DATE_FORMAT(r.updated_at, '%m') = DATE_FORMAT(CURRENT_TIMESTAMP(), '%m') - 1) then 1 else 0
              end
            ) as redeem_voucher_prev_month
          from 
            vouchers v
            left join redeem r on r.voucher_id = v.id
          where 
            v.deleted_at is null
        ),
        points_sum as (
          select
        	sum(point) as total_point,
        	sum(
              case 
                when (DATE_FORMAT(created_at, '%m') = DATE_FORMAT(CURRENT_TIMESTAMP(), '%m')) then 1 else 0
              end
            ) as total_point_last_month,
            sum(
              case 
                when (DATE_FORMAT(created_at, '%m') = DATE_FORMAT(CURRENT_TIMESTAMP(), '%m') - 1) then 1 else 0
              end
            ) as total_point_prev_month
          from 
          	points
          where
          	deleted_at is null
        ),
        points_redeemed_sum as (
          select
            sum(p.point) as total_point_redeemed,
            sum(
              case 
                when (DATE_FORMAT(p.created_at, '%m') = DATE_FORMAT(CURRENT_TIMESTAMP(), '%m')) then 1 else 0
              end
            ) as total_point_redeemed_last_month,
            sum(
              case 
                when (DATE_FORMAT(p.created_at, '%m') = DATE_FORMAT(CURRENT_TIMESTAMP(), '%m') - 1) then 1 else 0
              end
            ) as total_point_redeemed_prev_month
          from 
          	points p 
          	left join members m on m.phone_number = p.phone_number
          	left join redeem r on r.member_id = m.id
          where
          	p.deleted_at is null
          	and r.status = 'redeemed'
        )
        select 
          CAST(COALESCE(ms.total_members, 0) as integer) as total_members,
          CAST(COALESCE(round(ms.member_last_month_in_prec, 1), 0) as FLOAT) as member_last_month_in_prec,
          CAST(COALESCE(round(ms.member_prev_month_in_prec, 1), 0) as FLOAT) as member_prev_month_in_prec,
          CAST(COALESCE(vds.total_voucher_redeemed, 0) as INTEGER) as total_voucher_redeemed,
          CAST(COALESCE(round(vds.redeemed_voucher_last_month_in_prec, 1), 0) as FLOAT) as redeemed_voucher_last_month_in_prec,
          CAST(COALESCE(round(vds.redeemed_voucher_prev_month_in_prec, 1), 0) as FLOAT) as redeemed_voucher_prev_month_in_prec,
          CAST(COALESCE(vs.total_voucher_redeem, 0) as INTEGER) as total_voucher_redeem,
          CAST(COALESCE(round(vs.redeem_voucher_last_month_in_prec, 1), 0) as FLOAT) as redeem_voucher_last_month_in_prec,
          CAST(COALESCE(round(vs.redeem_voucher_prev_month_in_prec, 1), 0) as FLOAT) as redeem_voucher_prev_month_in_prec,
          CAST(COALESCE(ps.total_point, 0) as INTEGER) as total_point,
          CAST(COALESCE(round(ps.total_point_last_month_in_prec, 1), 0) as FLOAT) as total_point_last_month_in_prec,
          CAST(COALESCE(round(ps.total_point_prev_month_in_prec, 1), 0) as FLOAT) as total_point_prev_month_in_prec,
          CAST(COALESCE(pds.total_point_redeemed, 0) as INTEGER) as total_point_redeemed,
          CAST(COALESCE(round(pds.total_point_redeemed_last_month_in_prec, 1), 0) as FLOAT) as total_point_redeemed_last_month_in_prec,
          CAST(COALESCE(round(pds.total_point_redeemed_prev_month_in_prec, 1), 0) as FLOAT) as total_point_redeemed_prev_month_in_prec
        from 
          (
            select
              total_members,
              ((member_registred_last_month / total_members) * 100) as member_last_month_in_prec,
              ((member_registred_prev_month / total_members) * 100) as member_prev_month_in_prec
            from 
              members_sum
          ) ms,
          (
            select 
              total_voucher_redeemed,
              ((redeemed_voucher_last_month / total_voucher_redeemed) * 100) as redeemed_voucher_last_month_in_prec,
              ((redeemed_voucher_prev_month / total_voucher_redeemed) * 100) as redeemed_voucher_prev_month_in_prec
            from 
              vouchers_redeemed_sum
          ) vds,
          (
            select 
              total_voucher_redeem,
              ((redeem_voucher_last_month / total_voucher_redeem) * 100) as redeem_voucher_last_month_in_prec,
              ((redeem_voucher_prev_month / total_voucher_redeem) * 100) as redeem_voucher_prev_month_in_prec
            from 
              vouchers_redeem_sum
          ) vs,
          (
            select 
              total_point,
              ((total_point_last_month / total_point) * 100) as total_point_last_month_in_prec,
              ((total_point_prev_month / total_point) * 100) as total_point_prev_month_in_prec
            from 
              points_sum
          ) ps,
          (
            select 
              total_point_redeemed,
              ((total_point_redeemed_last_month / total_point_redeemed) * 100) as total_point_redeemed_last_month_in_prec,
              ((total_point_redeemed_prev_month / total_point_redeemed) * 100) as total_point_redeemed_prev_month_in_prec
            from 
              points_redeemed_sum
          ) pds
      ";

      $stmt = $this->conn->prepare($sql);
      $stmt->execute();
      $result = $stmt->get_result();
      $row = null;
      if ($result->num_rows > 0) {
        $row = $result->fetch_assoc();
      }

      $stmt->close();
      return $row;
    } catch (\Exception $e) {
      print_r($e);
    }
  }
}