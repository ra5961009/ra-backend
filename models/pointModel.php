<?php
namespace Models;
require_once("config.php");
use Models\Database;

class Point {
  private $db;
  private $conn;
  private $table = 'points';

  public function __construct() {
    $this->db = new Database();
    $this->conn = $this->db->connect();
  }
      
  public function getAll($limit, $offset, $search, $sort) {
    try {
      $src = '';
      $order = '';
      if ($search) $src .= "and (v.title like '%$search%' or v.code like '%$search%')";
      if (sizeof($sort) == 2) $order .= "ORDER BY $sort[0] $sort[1]";

      $sql = "
        SELECT 
          r.id, 
          m.id as member_id, 
          v.id as voucher_id, 
          v.code as voucher_code, 
          v.title as voucher_name, 
          v.points as voucher_points,
          v.expire_date,
          v.image_url as image_url,
          v.description as description,
          v.category as category,
          r.created_at,
          r.status,
          c.full_count
        FROM 
          $this->table r 
          left join members m on m.id=r.member_id 
          left join vouchers v on v.id=r.voucher_id
          RIGHT JOIN (SELECT count(*) AS full_count FROM redeem WHERE true and deleted_at is null $src) c ON true
        WHERE true
          and r.deleted_at is null
          $src
        $order
        LIMIT ?
        OFFSET ?
      ";

      $stmt = $this->conn->prepare($sql);
      $stmt->bind_param("ii", $limit, $offset);
      $stmt->execute();
      $result = $stmt->get_result();

      $rows = [];
      $total = 0;

      if ($result->num_rows > 0) {
        while($row = $result->fetch_assoc()) {
          if($total == 0) $total = $row["full_count"];
          unset($row["full_count"]);
          $rows[] = $row;
        }
      }
  
      $stmt->close();
      return ["rows" => $rows, "total" => $total];
    } catch (Exception $e) {
      throw $e;
    }
  }

  public function getByMemberId($id) {
    try {
      $sql = "
        SELECT 
          r.id, 
          m.id as member_id, 
          v.id as voucher_id, 
          v.code as voucher_code, 
          v.title as voucher_name, 
          v.points as voucher_points,
          r.created_at,
          r.status
        FROM 
          $this->table r 
          left join members m on m.id=r.member_id 
          left join vouchers v on v.id=r.voucher_id
        WHERE true
          and r.deleted_at is null
          and m.id = ?
      ";

      $stmt = $this->conn->prepare($sql);
      $stmt->bind_param("s", $id);
      $stmt->execute();
      $result = $stmt->get_result();

      $rows = [];
      if ($result->num_rows > 0) {
        while($row = $result->fetch_assoc()) {
          $rows[] = $row;
        }
      }

      $stmt->close();
      return $rows;
    } catch (Exception $e) {
      throw $e;
    }
  }

  public function create($params) {
    try {
      $sql = "INSERT INTO $this->table 
      (
        member_id,
        phone_number,
        point,
        operation,
        note
      ) VALUES (?,?,?,'out','redeem')";
      
      $stmt = $this->conn->prepare($sql);
      $stmt->bind_param("sss", 
         $params->member_id, 
         $params->phone_number,
         $params->point
      );
      
      $stmt->execute();
    
      $this->updatePointMember($params);
      $this->updateQtyVoucher($params);
      
      $result = $stmt->get_result();
      return $result;
    } catch (\Exception $e) {
        print_r($e);
    }
  }
  
  public function updatePointMember($params) {
    try {
      $sql = "UPDATE members SET total_point_balance = total_point_balance - (SELECT points FROM vouchers WHERE id = ?) WHERE id = ?";
      $stmt = $this->conn->prepare($sql);
      $stmt->bind_param("ss", 
      $params->voucher_id,
      $params->member_id
      );
      $stmt->execute();
    } catch (\Exception $e) {
        print_r($e);
    }
  }
  
  public function updateQtyVoucher($params) {
    try {
        $sql = "UPDATE vouchers SET status = 'redeem', qty = (qty - 1) WHERE id = ?";
        
        $stmt = $this->conn->prepare($sql);
        $stmt->bind_param("s", 
             $params->voucher_id
        );
        $stmt->execute();
    } catch (\Exception $e) {
        print_r($e);
    }
  }

  public function redeem($params) {
    try {
      $sql = "UPDATE redeem SET status = 'redeemed', updated_at=now() WHERE id = ?";
      $stmt = $this->conn->prepare($sql);
      $stmt->bind_param("s", 
        $params
      );
      $stmt->execute();
      return $params;
    } catch (\Exception $e) {
      print_r($e);
    }
  }
}