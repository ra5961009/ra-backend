<?php
namespace Models;
require_once("config.php");
use Models\Database;

class Transaction {
  private $db;
  private $conn;
  private $table = 'transactions';

  public function __construct() {
    $this->db = new Database();
    $this->conn = $this->db->connect();
  }

  public function getAll ($limit, $offset, $search, $sort, $filters, $query) {
    try {
      $src = '';
      $order = '';
      $filter = '';

      $src_filter = str_replace('%20', ' ', $search);
      if ($search) $src .= "and (h.name like '%$src_filter%' or m.first_name like '%$src_filter%' or m.last_name like '%$src_filter%' or t.transaction_type like '%$src_filter%')";
      if (sizeof($sort) > 0) {
        $order .= "ORDER BY ";
        foreach($sort as $s) {
          $ss = explode(":",$s);
          $order .= "$ss[0] $ss[1]".",";
        }

        $order = rtrim($order, ",");
      }

      if (sizeof($filters) > 0) {
        foreach($filters as $f) {
          $fa = explode(':', $f);
          $arr = explode(",", $fa[1]);
          $arr = array_map(function($str) { return str_replace('%20', ' ', $str); }, $arr);
          $column = $fa[0];
          $values = "('" . implode("','", $arr) . "') ";
          $filter .= " and $fa[0] in $values";
        }
      }

      if (isset($query['start_date']) && isset($query['end_date'])) {
        if ($query['start_date']!= "" && $query['end_date']!= "") {
          $start = $query['start_date'];
          $end = $query['end_date'];
          $filter .= " and date(t.created_at) >= '$start' AND date(t.created_at) <= '$end'";
        }
      }

      if (isset($query['transaction_type'])) {
        if ($query['transaction_type']!= "") {
          $types = "('" . implode("','", explode(",", $query['transaction_type'])) . "') ";
          $filter .= " and t.transaction_type in $types";
        }
      }

      $sql = "
        SELECT 
          t.member_code,
          concat(m.first_name, ' ' ,m.last_name) as full_name,
          m.phone_number,
          h.name as hotel_name,
          t.invoice_number,
          t.total_value,
          t.transaction_type,
          s.name as store_name,
          floor(t.total_value / 100000) as point_plus,
          t.created_at,
          c.full_count 
        FROM 
          $this->table t
          left join members m on m.code = t.member_code 
          left join hotels h on h.code = t.hotel_code
          left join outlet s on s.code = t.store and s.hotels_id = h.id
          RIGHT JOIN (SELECT count(*) AS full_count FROM $this->table t WHERE true and t.deleted_at is null $filter) c ON true
        WHERE true
          and t.deleted_at is null
          $filter
          $src
        $order
        LIMIT ?
        OFFSET ?
      ";

      $stmt = $this->conn->prepare($sql);
      $stmt->bind_param("ii", $limit, $offset);
      $stmt->execute();
      $result = $stmt->get_result();

      $rows = [];
      $total = 0;

      if ($result->num_rows > 0) {
        while($row = $result->fetch_assoc()) {
          if($total == 0) $total = $row["full_count"];
          unset($row["full_count"]);
          $rows[] = $row;
        }
      }
  
      $stmt->close();
      return ["rows" => $rows, "total" => $total];
    } catch (Exception $e) {
      throw $e;
    }
  }

  public function getById ($id) {
    $sql = "SELECT * FROM $this->table WHERE id = ?";
    $stmt = $this->conn->prepare($sql);
    $stmt->bind_param("s", $id);
    $stmt->execute();
    $result = $stmt->get_result();

    $row = null;
    if ($result->num_rows > 0) {
      $row = $result->fetch_assoc();
    }

    $stmt->close();
    return $row;
  }
}