<?php
namespace Models;
require_once("config.php");
use Models\Database;

class Benefits {
  private $db;
  private $conn;
  private $table = 'benefits';

  public function __construct() {
    $this->db = new Database();
    $this->conn = $this->db->connect();
  }

    public function create($params) {
    try {
      $sql = "INSERT INTO $this->table 
      (
        hotels_id,
        outlet_code,
        type,
        article_number,
        title,
        description,
        plu,
        discount,
        price,
        category,
        code,
        qty,
        expire_date,
        image_url
      ) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?) RETURNING id";
      $stmt = $this->conn->prepare($sql);
      $stmt->bind_param("sssssssiississ", 
        $params->hotels_id,
        $params->outlet_code,
        $params->type,
        $params->article_number,
        $params->title,
        $params->description,
        $params->plu,
        $params->discount,
        $params->price,
        $params->category,
        $params->code,
        $params->qty,
        $params->expire_date,
        $params->image_url
      );
      
      $stmt->execute();
      $result = $stmt->get_result();
      return $result;
    } catch (\Exception $e) {
      throw $e;
    }
  }

  public function edit($body, $param) {
    try {
      $sql = "UPDATE $this->table SET 
        hotels_id = ?,
        outlet_code = ?,
        type = ?,
        article_number = ?,
        title = ?,
        description = ?,
        plu = ?,
        discount = ?,
        price = ?,
        category = ?,
        code = ?,
        qty = ?,
        expire_Date = ?,
        image_url = ?
      WHERE id=?";

      $stmt = $this->conn->prepare($sql);
      $stmt->bind_param("sssssssiississs", 
        $body->hotels_id,
        $body->outlet_code,
        $body->type,
        $body->article_number,
        $body->title,
        $body->description,
        $body->plu,
        $body->discount,
        $body->price,
        $body->category,
        $body->code,
        $body->qty,
        $body->expire_date,
        $body->image_url,
        $param
      );
      
      if ($stmt->execute()) {
        $body->id = $param;
        return $body;
      }
    } catch (\Exception $e) {
      throw $e;
    }
  }

   public function _delete($param) {
    try {
      $sql = "UPDATE $this->table SET deleted_at=now() WHERE id=?";
      $stmt = $this->conn->prepare($sql);
      $stmt->bind_param("s", $param);
      
      if ($stmt->execute()) {
        return $param;
      }
    } catch (\Exception $e) {
      throw $e;
    }
  }

  public function getById($id) {
    $sql = "SELECT * FROM $this->table WHERE id = ?";
    $stmt = $this->conn->prepare($sql);
    $stmt->bind_param("s", $id);
    $stmt->execute();
    $result = $stmt->get_result();

    $row = null;
    if ($result->num_rows > 0) {
      $row = $result->fetch_assoc();
    }

    $stmt->close();
    return $row;
  }
  
   public function getAll($limit, $offset, $search, $sort) {
    try {
      $src = '';
      $order = '';
      if ($search) $src .= "and (code like '%$search%' or title like '%$search%')";
      if (sizeof($sort) == 2) $order .= "ORDER BY $sort[0] $sort[1]";

      $sql = "
        SELECT 
          *, 
          c.full_count 
        FROM 
          $this->table 
          RIGHT JOIN (SELECT count(*) AS full_count FROM $this->table WHERE true and deleted_at is null $src) c ON true
        WHERE true
          and deleted_at is null
          $src
        $order
        LIMIT ?
        OFFSET ?
      ";

      $stmt = $this->conn->prepare($sql);
      $stmt->bind_param("ii", $limit, $offset);
      $stmt->execute();
      $result = $stmt->get_result();

      $rows = [];
      $total = 0;

      if ($result->num_rows > 0) {
        while($row = $result->fetch_assoc()) {
          if($total == 0) $total = $row["full_count"];
          unset($row["full_count"]);
          $rows[] = $row;
        }
      }
  
      $stmt->close();
      return ["rows" => $rows, "total" => $total];
    } catch (\Exception $e) {
      throw $e;
    }
  }

  public function getByHotelId($id) {
    $sql = "SELECT * FROM $this->table WHERE hotels_id = ?";
    $stmt = $this->conn->prepare($sql);
    $stmt->bind_param("s", $id);
    $stmt->execute();
    $result = $stmt->get_result();

    $rows = [];
    if ($result->num_rows > 0) {
      while($row = $result->fetch_assoc()) {
          $rows[] = $row;
        }
    }

    $stmt->close();
    return $rows;
  }
}