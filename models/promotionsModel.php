<?php
namespace Models;
require_once("config.php");
use Models\Database;

class Promotions {
  private $db;
  private $conn;
  private $table = 'promotions';

  public function __construct() {
    $this->db = new Database();
    $this->conn = $this->db->connect();
  }

  public function create($params) {
    try {
      $sql = "INSERT INTO $this->table 
      (
        id,
        hotels_id,
        name,
        description,
        img_url
      ) VALUES (uuid(), ?,?,?,?)";
      $stmt = $this->conn->prepare($sql);
      $stmt->bind_param("ssss", 
        $params->hotels_id,
        $params->name, 
        $params->description, 
        $params->img_url
      );
      
      $stmt->execute();
      $result = $stmt->get_result();
      return $result;
    } catch (\Exception $e) {
      throw $e;
    }
  }

  public function edit($body, $param) {
    try {
      $sql = "UPDATE $this->table SET 
        hotels_id=?,
        name=?,
        description=?,
        img_url=?
      WHERE id=?";

      $stmt = $this->conn->prepare($sql);
      $stmt->bind_param("sssss", 
        $body->hotels_id,
        $body->name, 
        $body->description, 
        $body->img_url,
        $param
      );
      
      if ($stmt->execute()) {
        $body->id = $param;
        return $body;
      }
    } catch (\Exception $e) {
      throw $e;
    }
  }

  public function _delete($param) {
    try {
      $sql = "UPDATE $this->table SET deleted_at=now() WHERE id=?";
      $stmt = $this->conn->prepare($sql);
      $stmt->bind_param("s", $param);
      
      if ($stmt->execute()) {
        return $param;
      }
    } catch (\Exception $e) {
      throw $e;
    }
  }

  public function getAll () {
    try {
      $sql = "SELECT * FROM $this->table WHERE true and deleted_at is null order by created_at desc";
      $stmt = $this->conn->query($sql);
  
      $rows = [];
      if ($stmt->num_rows > 0) {
        while($row = $stmt->fetch_assoc()) {
          $rows[] = $row;
        }
      }
  
      $stmt->close();
      return $rows;
    } catch (Exception $e) {
      throw $e;
    }
  }
  
  public function getById ($id) {
    $sql = "SELECT * FROM $this->table WHERE id = ?";
    $stmt = $this->conn->prepare($sql);
    $stmt->bind_param("s", $id);
    $stmt->execute();
    $result = $stmt->get_result();

    $row = null;
    if ($result->num_rows > 0) {
      $row = $result->fetch_assoc();
    }

    $stmt->close();
    return $row;
  }
  
  public function getByHotelsId ($id) {
      try {
        $sql = "SELECT * FROM $this->table WHERE hotels_id = ?";
        $stmt = $this->conn->prepare($sql);
        $stmt->bind_param("s", $id);
        $stmt->execute();
        $result = $stmt->get_result();
    
        $rows = [];
        if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {
              $rows[] = $row;
            }
        }
    
        $stmt->close();
        return $rows;
      } catch (Exception $e) {
        throw $e;
      }
  }
}