<?php
namespace Models;
require_once("config.php");
use Models\Database;

class User {
  private $db;
  private $conn;
  private $table = 'users';

  public function __construct() {
    $this->db = new Database();
    $this->conn = $this->db->connect();
  }

  public function getAll ($limit, $offset, $search, $sort) {
    try {
      $src = '';
      $order = '';
      if ($search) $src .= "and (name like '%$search%' or email like '%$search%')";
      if (sizeof($sort) == 2) $order .= "ORDER BY $sort[0] $sort[1]";

      $sql = "
        SELECT 
          *, 
          c.full_count 
        FROM 
          $this->table 
          RIGHT JOIN (SELECT count(*) AS full_count FROM $this->table WHERE true $src) c ON true
        WHERE true
          and deleted_at is null
          $src
        $order
        LIMIT ?
        OFFSET ?
      ";

      $stmt = $this->conn->prepare($sql);
      $stmt->bind_param("ii", $limit, $offset);
      $stmt->execute();
      $result = $stmt->get_result();

      $rows = [];
      $total = 0;

      if ($result->num_rows > 0) {
        while($row = $result->fetch_assoc()) {
          if($total == 0) $total = $row["full_count"];
          unset($row["full_count"]);
          $rows[] = $row;
        }
      }
  
      $stmt->close();
      return ["rows" => $rows, "total" => $total];
    } catch (Exception $e) {
      throw $e;
    }
  }


  public function getById ($id) {
    $sql = "SELECT id, name, email, role, created_at, updated_at FROM $this->table WHERE id = ?";
    $stmt = $this->conn->prepare($sql);
    $stmt->bind_param("s", $id);
    $stmt->execute();
    $result = $stmt->get_result();

    $row = null;
    if ($result->num_rows > 0) {
      $row = $result->fetch_assoc();
    }

    $stmt->close();
    return $row;
  }

  public function getByEmail ($email) {
    $sql = "SELECT id, name, email, password, role, created_at, updated_at FROM users WHERE email = ?";
    $stmt = $this->conn->prepare($sql);
    $stmt->bind_param("s", $email);
    $stmt->execute();
    $result = $stmt->get_result();

    $row = null;
    if ($result->num_rows > 0) {
      $row = $result->fetch_assoc();
    }

    $stmt->close();
    return $row;
  }

  public function create ($params) {
    try {
      $sql = "INSERT INTO $this->table (name,email,password,role) VALUES (?,?,?,?)";
      $stmt = $this->conn->prepare($sql);
      $stmt->bind_param("ssss", $params->name, $params->email, $params->password, $params->role);
      $stmt->execute();
      $result = $stmt->get_result();
      return $result;
    } catch (Exception $e) {
      print_r($e);
    }
  }

  public function edit ($body, $param) {
    try {
      $sql = "UPDATE $this->table SET name=?, role=? WHERE id=?";
      $stmt = $this->conn->prepare($sql);
      $stmt->bind_param("sss", $body->name, $body->role, $param);
      $stmt->execute();
      $stmt->close();

      return true;
    } catch (Exception $e) {
      print_r($e);
      throw $e;
    }
  }

  public function _delete($param) {
    try {
      $sql = "UPDATE $this->table SET deleted_at=now() WHERE id=?";
      $stmt = $this->conn->prepare($sql);
      $stmt->bind_param("s", $param);
      
      if ($stmt->execute()) {
        return $param;
      }
    } catch (\Exception $e) {
      throw $e;
    }
  }
}