<?php
namespace Models;
require_once("config.php");
use Models\Database;

class Notification {
  private $db;
  private $conn;
  private $table = 'notification';

  public function __construct() {
    $this->db = new Database();
    $this->conn = $this->db->connect();
  }

  public function create($params) {
    try {
      $sql = "INSERT INTO $this->table 
      (
        id,
        phone_number,
        type,
        title,
        description,
        is_read,
        platform
      ) VALUES (uuid(), ?,?,?,?,?,?) RETURNING id";
      $stmt = $this->conn->prepare($sql);
      $stmt->bind_param("ssssss", 
        $params->phone_number,
        $params->type,
        $params->title,
        $params->body,
        $params->is_read,
        $params->platform
      );
      
      $stmt->execute();
      $result = $stmt->get_result();
      
      $row = null;
      if ($result->num_rows > 0) {
        $row = $result->fetch_assoc();
      }
      
      $stmt->close();
      return $row;
    } catch (Exception $e) {
      throw $e;
    }
  }

  public function getAll ($limit, $offset, $search, $sort, $filters) {
    try {
      $src = '';
      $order = '';
      $filter = '';

      if ($search) $src .= "and (title like '%$search%')";
      if (sizeof($sort) > 0) {
        $order .= "ORDER BY ";
        foreach($sort as $s) {
          $ss = explode(":",$s);
          $order .= "$ss[0] $ss[1]".",";
        }

        $order = rtrim($order, ",");
      }

      if (sizeof($filters) > 0) {
        foreach($filters as $f) {
          $fa = explode(':', $f);
          $arr = explode(",", $fa[1]);
          $arr = array_map(function($str) { return str_replace('%20', ' ', $str); }, $arr);
          $column = $fa[0];
          $values = "('" . implode("','", $arr) . "') ";
          $filter .= " and $fa[0] in $values";
        }
      }
      
      $sql = "
        SELECT 
          *, 
          c.full_count 
        FROM 
          $this->table 
          RIGHT JOIN (SELECT count(*) AS full_count FROM $this->table WHERE true $filter $src) c ON true
        WHERE true
          $filter
          $src
        $order
        LIMIT ?
        OFFSET ?
      ";

      $stmt = $this->conn->prepare($sql);
      $stmt->bind_param("ii", $limit, $offset);
      $stmt->execute();
      $result = $stmt->get_result();

      $rows = [];
      $total = 0;

      if ($result->num_rows > 0) {
        while($row = $result->fetch_assoc()) {
          if($total == 0) $total = $row["full_count"];
          unset($row["full_count"]);
          $rows[] = $row;
        }
      }
  
      $stmt->close();
      return ["rows" => $rows, "total" => $total];
    } catch (Exception $e) {
      throw $e;
    }
  }
}