<?php
namespace Models;
require_once("config.php");
use Models\Database;

class Outlet {
  private $db;
  private $conn;
  private $table = 'outlet';

  public function __construct() {
    $this->db = new Database();
    $this->conn = $this->db->connect();
  }
  
  public function getByHotelsId ($id) {
    $sql = "SELECT * FROM $this->table WHERE hotels_id = ?";
    $stmt = $this->conn->prepare($sql);
    $stmt->bind_param("s", $id);
    $stmt->execute();
    $result = $stmt->get_result();

    $rows = [];
    if ($result->num_rows > 0) {
      while($row = $result->fetch_assoc()) {
          $rows[] = $row;
        }
    }

    $stmt->close();
    return $rows;
  }
}