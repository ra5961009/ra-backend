<?php
include_once("controllers/propertyController.php");
include_once("middleware/auth.php");
include_once("utils/error.php");

use Controller\PropertyController;
$propertyController = new PropertyController();
$param = isset($pathParts[3]) ? $pathParts[3] : '';

switch ($_SERVER['REQUEST_METHOD']) {
    case "GET":
        both();
        if ($param) {
            $result = $propertyController->getById($param);
            http_response_code(200);
            echo json_encode(array("property" => $result));
        } else {
            $results = $propertyController->getAll();
            http_response_code(200);
            echo json_encode(array("property" => $results));
        }
        break;
    case "POST":
        both();
        try {
            $results = $propertyController->create($body);
            http_response_code(201);
            echo json_encode(array("property" => $results));
        } catch (Exception $e) {
            error($e);
        }
        break;
    case "PUT":
        both();
        try {
            $results = $propertyController->edit($body, $param);
            http_response_code(200);
            echo json_encode(array("property" => $results));
        } catch (Exception $e) {
            error($e);
        }
        break;
    case "DELETE":
        both();
        try {
            $results = $propertyController->_delete($param);
            http_response_code(200);
            echo json_encode(array("property" => $results));
        } catch (Exception $e) {
            error($e);
        }
        break;
    default:
        http_response_code(405);
        echo json_encode(array("message" => "Method not allowed"));
}