<?php
include_once("controllers/benefitsController.php");
include_once("middleware/auth.php");
include_once("utils/error.php");

use Controller\BenefitsController;
$benefitsController = new BenefitsController();
$param = isset($pathParts[3]) ? $pathParts[3] : '';

switch ($_SERVER['REQUEST_METHOD']) {
    case "GET":
        both();
        if ($param) {
            $result = $benefitsController->getByHotelId($param);
            http_response_code(200);
            echo json_encode(array("benefits" => $result));
        } else {
            $results = $benefitsController->getAll($query);
            http_response_code(200);
            echo json_encode($results);
        }
        break;
    case "POST":
        both();
        try {
            $benefitsController->create($body);
        } catch (Exception $e) {
            error($e);
        }
        break;
    case "PUT":
        both();
        try {
            $results = $benefitsController->edit($body, $param);
            http_response_code(200);
            echo json_encode(array("benefits" => $results));
        } catch (Exception $e) {
            error($e);
        }
        break;
    case "DELETE":
        both();
        try {
            $results = $benefitsController->_delete($param);
            http_response_code(200);
            echo json_encode(array("benefits" => $results));
        } catch (Exception $e) {
            error($e);
        }
        break;
    default:
        http_response_code(405);
        echo json_encode(array("message" => "Method not allowed"));
}