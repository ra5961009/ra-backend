<?php
include_once("controllers/transactionController.php");
include_once("middleware/auth.php");
include_once("utils/error.php");

use Controller\TransactionController;
$transactionController = new TransactionController();
$param = isset($pathParts[3]) ? $pathParts[3] : '';

switch ($_SERVER['REQUEST_METHOD']) {
    case "GET":
        if ($param) {
            $result = $transactionController->getById($param);
            http_response_code(200);
            echo json_encode(array("transaction" => $result));
        } else {
            $results = $transactionController->getAll($query);
            http_response_code(200);
            echo json_encode($results);
        }
        break;
    default:
        http_response_code(405);
        echo json_encode(array("message" => "Method not allowed"));
}