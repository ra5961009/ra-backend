<?php
include_once("controllers/cardController.php");
include_once("middleware/auth.php");
include_once("utils/error.php");

use Controller\CardController;
$cardController = new CardController();
$param = isset($pathParts[3]) ? $pathParts[3] : '';

switch ($_SERVER['REQUEST_METHOD']) {
    case "GET":
        both();
        if ($param) {
            $result = $cardController->getById($param);
            http_response_code(200);
            echo json_encode(array("card" => $result));
        } else {
            $cardController->getAll();
        }
        break;
    case "POST":
        both();
        try {
            $results = $cardController->create($body);
            http_response_code(201);
            echo json_encode(array("card" => $results));
        } catch (Exception $e) {
            error($e);
        }
        break;
    case "PUT":
        both();
        try {
            $results = $cardController->edit($body, $param);
            http_response_code(200);
            echo json_encode(array("card" => $results));
        } catch (Exception $e) {
            error($e);
        }
        break;
    case "DELETE":
        both();
        try {
            $results = $cardController->_delete($param);
            http_response_code(200);
            echo json_encode(array("card" => $results));
        } catch (Exception $e) {
            error($e);
        }
        break;
    default:
        http_response_code(405);
        echo json_encode(array("message" => "Method not allowed"));
}