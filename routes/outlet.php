<?php
include_once("controllers/outletController.php");
include_once("middleware/auth.php");
include_once("utils/error.php");

use Controller\OutletController;
$outletController = new OutletController();
$param = isset($pathParts[3]) ? $pathParts[3] : '';

switch ($_SERVER['REQUEST_METHOD']) {
    case "GET":
        both();
        if ($param) {
            $result = $outletController->getByHotelsId($param);
            http_response_code(200);
            echo json_encode(array("outlets" => $result));
        }
        break;
    default:
        http_response_code(405);
        echo json_encode(array("message" => "Method not allowed"));
}