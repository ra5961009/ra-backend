<?php
include_once("controllers/facilitiesController.php");
include_once("middleware/auth.php");
include_once("utils/error.php");

use Controller\FacilitiesController;
$facilitiesController = new FacilitiesController();
$param = isset($pathParts[3]) ? $pathParts[3] : '';

switch ($_SERVER['REQUEST_METHOD']) {
    case "GET":
        both();
        if ($param) {
            $result = $facilitiesController->getByHotelsId($param);
            http_response_code(200);
            echo json_encode(array("facilities" => $result));
        } else {
            $results = $facilitiesController->getAll();
            http_response_code(200);
            echo json_encode(array("facilities" => $results));
        }
        break;
    case "POST":
        both();
        try {
            $results = $facilitiesController->create($body);
            http_response_code(201);
            echo json_encode(array("facilities" => $results));
        } catch (Exception $e) {
            error($e);
        }
        break;
    case "PUT":
        both();
        try {
            $results = $facilitiesController->edit($body, $param);
            http_response_code(200);
            echo json_encode(array("facilities" => $results));
        } catch (Exception $e) {
            error($e);
        }
        break;
    case "DELETE":
        both();
        try {
            $results = $facilitiesController->_delete($param);
            http_response_code(200);
            echo json_encode(array("facilities" => $results));
        } catch (Exception $e) {
            error($e);
        }
        break;
    default:
        http_response_code(405);
        echo json_encode(array("message" => "Method not allowed"));
}