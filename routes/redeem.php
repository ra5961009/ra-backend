<?php
include_once("controllers/redeemController.php");
include_once("middleware/auth.php");
include_once("utils/error.php");

use Controller\RedeemController;
$redeemController = new RedeemController();
$param = isset($pathParts[3]) ? $pathParts[3] : '';
switch ($_SERVER['REQUEST_METHOD']) {
    case "GET":
        both();
        if ($param) {
            $result = $redeemController->getByMemberId($param);
            http_response_code(200);
            echo json_encode(array("redeem" => $result));
        } else {
            $result = $redeemController->getAll($query);
        }
        break;
    case "POST":
        both();
        try {
            $redeemController->create($body);
        } catch (Exception $e) {
            error($e);
        }
        break;
    case "PUT":
        both();
        try {
            $results = $redeemController->redeem($body, $param);
            http_response_code(200);
            echo json_encode(array("redeem" => $results));
        } catch (Exception $e) {
            error($e);
        }
        break;    
    default:
        http_response_code(405);
        echo json_encode(array("message" => "Method not allowed"));
}