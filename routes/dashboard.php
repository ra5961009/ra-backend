<?php
include_once("controllers/dashboardController.php");
include_once("middleware/auth.php");
include_once("utils/error.php");

use Controller\DashboardController;

$dashboardController = new DashboardController();
$path = isset($pathParts[3]) ? $pathParts[3] : '';

both();
switch ($_SERVER['REQUEST_METHOD']) {
  case "GET":
    try {
      if(!isset($pathParts[4])) {
        switch($path) {
          case 'summary':
            $dashboardController->summary();
          break;
          case 'bars':
            $dashboardController->bars();
          break;
          default:
            http_response_code(404);
            echo json_encode(array("message" => "Page not found"));        
        }
      } else {
        http_response_code(404);
        echo json_encode(array("message" => "Page not found"));        
      }
    } catch (\Exception $e) {
      error($e);
    }
    break;
  default:
    http_response_code(405);
    echo json_encode(array("message" => "Method not allowed"));
}