<?php
include_once("controllers/voucherController.php");
include_once("middleware/auth.php");
include_once("utils/error.php");

use Controller\VoucherController;
$voucherController = new VoucherController();
$param = isset($pathParts[3]) ? $pathParts[3] : '';

switch ($_SERVER['REQUEST_METHOD']) {
    case "GET":
        both();
        if ($param) {
            $result = $voucherController->getByHotelId($param);
            http_response_code(200);
            echo json_encode(array("voucher" => $result));
        } else {
            $results = $voucherController->getAll($query);
            http_response_code(200);
            echo json_encode($results);
        }
        break;
    case "POST":
        both();
        try {
            $voucherController->create($body);
        } catch (Exception $e) {
            error($e);
        }
        break;
    case "PUT":
        both();
        try {
            $results = $voucherController->edit($body, $param);
            http_response_code(200);
            echo json_encode(array("voucher" => $results));
        } catch (Exception $e) {
            error($e);
        }
        break;
    case "DELETE":
        both();
        try {
            $results = $voucherController->_delete($param);
            http_response_code(200);
            echo json_encode(array("voucher" => $results));
        } catch (Exception $e) {
            error($e);
        }
        break;
    default:
        http_response_code(405);
        echo json_encode(array("message" => "Method not allowed"));
}