<?php
include_once("controllers/notificationController.php");
include_once("middleware/auth.php");
include_once("utils/error.php");

use Controller\NotificationController;
$notificationController = new NotificationController();
$param = isset($pathParts[3]) ? $pathParts[3] : '';

switch ($_SERVER['REQUEST_METHOD']) {
    case "GET":
        both();
        if ($param) {
            $result = $notificationController->getByHotelsId($param);
            http_response_code(200);
            echo json_encode(array("notification" => $result));
        } else {
            $results = $notificationController->getAll($query);
        }
        break;
    default:
        http_response_code(405);
        echo json_encode(array("message" => "Method not allowed"));
}