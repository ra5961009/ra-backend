<?php
include_once("controllers/userController.php");
include_once("middleware/auth.php");
include_once("utils/error.php");

use Controller\UserController;
$userController = new UserController();
$param = isset($pathParts[3]) ? $pathParts[3] : '';

web();
switch ($_SERVER['REQUEST_METHOD']) {
    case "GET":
        both();
        if ($param) {
            $result = $userController->getById($param);
            http_response_code(200);
            echo json_encode(array("user" => $result));
        } else {
            $results = $userController->getAll($query);
            http_response_code(200);
            echo json_encode($results);
        }
        break;
    case "POST":
        both();
        try {
            $results = $userController->register($body);
            http_response_code(201);
            echo json_encode(array("users" => $results));
        } catch (Exception $e) {
            error($e);
        }
        break;
    case "PUT":
        try {
            $userController->edit($body, $param);
        } catch (Exception $e) {
            error($e);
        }
        break;
    case "DELETE":
        try {
            $results = $userController->_delete($param);
            http_response_code(200);
            echo json_encode(array("user" => $results));
        } catch (Exception $e) {
            error($e);
        }
        break;
    default:
        http_response_code(405);
        echo json_encode(array("message" => "Method not allowed"));
}