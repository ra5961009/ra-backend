<?php
include_once("controllers/promotionsController.php");
include_once("middleware/auth.php");
include_once("utils/error.php");

use Controller\PromotionsController;
$promotionsController = new PromotionsController();
$param = isset($pathParts[3]) ? $pathParts[3] : '';

switch ($_SERVER['REQUEST_METHOD']) {
    case "GET":
        both();
        if ($param) {
            $result = $promotionsController->getByHotelsId($param);
            http_response_code(200);
            echo json_encode(array("promotions" => $result));
        } else {
            $results = $promotionsController->getAll();
            http_response_code(200);
            echo json_encode(array("promotions" => $results));
        }
        break;
    case "POST":
        both();
        try {
            $results = $promotionsController->create($body);
        } catch (Exception $e) {
            error($e);
        }
        break;
    case "PUT":
        both();
        $results = $promotionsController->edit($body, $param);
        break;
    case "DELETE":
        both();
        try {
            $results = $promotionsController->_delete($param);
            http_response_code(200);
            echo json_encode(array("promotions" => $results));
        } catch (Exception $e) {
            error($e);
        }
        break;
    default:
        http_response_code(405);
        echo json_encode(array("message" => "Method not allowed"));
}