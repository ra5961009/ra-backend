<?php
include_once("controllers/memberController.php");
include_once("middleware/auth.php");
include_once("utils/error.php");

use Controller\MemberController;
$memberController = new MemberController();
$param = isset($pathParts[3]) ? $pathParts[3] : '';

switch ($_SERVER['REQUEST_METHOD']) {
    case "GET":
        if ($param == 'get_starting_point') {
            $result = $memberController->get_starting_point();
        } else {
            if ($param) {
                $result = $memberController->getById($param);
                http_response_code(200);
                echo json_encode(array("member" => $result));
            } else {
                $results = $memberController->getAll($query);
                http_response_code(200);
                echo json_encode($results);
            }
        }
        break;
    case "POST":
        try {
            $results = $memberController->create($body);
        } catch (Exception $e) {
            error($e);
        }
        break;
    case "PUT":
        try {
            if ($param == 'set_starting_point') {
                $result = $memberController->set_starting_point($body);
            } else if($param == 'update_active') {
                $result = $memberController->updateActive($body);
            } else {
                $results = $memberController->edit($body, $param);
                http_response_code(200);
                echo json_encode(array("member" => $results));
            }
        } catch (Exception $e) {
            error($e);
        }
        break;
    case "DELETE":
        
        try {
            $results = $memberController->_delete($param);
            http_response_code(200);
            echo json_encode(array("member" => $results));
        } catch (Exception $e) {
            error($e);
        }
        break;
    default:
        http_response_code(405);
        echo json_encode(array("message" => "Method not allowed"));
}