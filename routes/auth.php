<?php
include_once("controllers/authController.php");
include_once("utils/error.php");

use Controller\UserController;
$authController = new UserController();

switch ($_SERVER['REQUEST_METHOD']) {
  case 'POST':
    $param = isset($pathParts[3]) ? $pathParts[3] : '';
    if ($param == "login") {
      try {
        $result = $authController->login($body);
        http_response_code(200);
        echo json_encode(array("profile" => $result));
      } catch (Exception $e) {
        error($e);
      }
    } else if($param == "forgot") {
      try {
        $result = $authController->forgot($body);
        http_response_code(200);
        echo json_encode(array("forgot" => $result));
      } catch (Exception $e) {
        error($e);
      }
    } else if($param === "logout") {
      try {
        $result = $authController->logout($body);
        http_response_code(200);
        echo json_encode(array("logout" => $result));
      } catch (Exception $e) {
        error($e);
      }
    }else {
      http_response_code(404);
      echo json_encode(array("message" => "Endpoint not found"));  
    }
    break;
  default:
    http_response_code(405);
    echo json_encode(array("message" => "Method not allowed"));
}