<?php
namespace Controller;
require_once("models/propertyModel.php");

use Models\Property;

class PropertyController {
 
  public function create($body)  {
    try {
      $property = new Property();
      $result = $property->create($body);
      return $body;
    } catch (\Exception $e) {
      throw $e;
    }
  }

  public function edit($body, $param)  {
    try {
      $property = new Property();
      $oldData = $property->getById($param);
      
      $body->code = $body->code ?? $oldData['code'];
      $body->name = $body->name ?? $oldData['name']; 
      $body->address = $body->address ?? $oldData['address']; 
      $body->phone = $body->phone ?? $oldData['phone']; 
      $body->email = $body->email ?? $oldData['email']; 
      $body->linktree_chat = $body->linktree_chat ?? $oldData['linktree_chat']; 
      $body->about = $body->about ?? $oldData['about']; 
      $body->thumbnail_url = $body->thumbnail_url ?? $oldData['thumbnail_url'];
      
      $result = $property->edit($body, $param);
      return $result;
    } catch (\Exception $e) {
      throw $e;
    }
  }

  public function _delete($param)  {
    try {
      $property = new Property();
      $property->_delete($param);

      $result = ["id" => $param];
      return $result;
    } catch (\Exception $e) {
      throw $e;
    }
  }

  public function getAll() {
    try {
      $property = new Property();
      $result = $property->getAll();
      return $result;
    } catch (\Exception $e) {
      throw $e;
    }
  }
  
  public function getById($id) {
    try {
      $property = new Property();
      $result = $property->getById($id);
      return $result;
    } catch (\Exception $e) {
      throw $e;
    }
  }

}