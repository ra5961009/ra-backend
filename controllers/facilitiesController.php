<?php
namespace Controller;
require_once("models/facilitiesModel.php");

use Models\Facilities;

class FacilitiesController {
 
  public function create($body)  {
    try {
      $facilities = new Facilities();
      $result = $facilities->create($body);
      return $body;
    } catch (\Exception $e) {
      throw $e;
    }
  }

  public function edit($body, $param)  {
    try {
      $facilities = new Facilities();
      $oldData = $facilities->getById($param);
      
      $body->hotels_id = $body->hotels_id ?? $oldData['hotels_id'];
      $body->name = $body->name ?? $oldData['name']; 
      $body->img_url = $body->img_url ?? $oldData['img_url']; 
      
      $result = $facilities->edit($body, $param);
      return $result;
    } catch (\Exception $e) {
      throw $e;
    }
  }

  public function _delete($param)  {
    try {
      $facilities = new Facilities();
      $facilities->_delete($param);

      $result = ["id" => $param];
      return $result;
    } catch (\Exception $e) {
      throw $e;
    }
  }

  public function getAll() {
    $facilities = new Facilities();
    $result = $facilities->getAll();
    return $result;
  }
  
  public function getByHotelsId($id) {
    $facilities = new Facilities();
    $result = $facilities->getByHotelsId($id);
    return $result;
  }
}