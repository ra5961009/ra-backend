<?php
namespace Controller;
require_once("models/transactionModel.php");

use Models\Transaction;
use stdClass;

class TransactionController {
  public function getAll($query) {
    $page = isset($query['page']) ? intval($query['page']) : 1;
    $per_page = isset($query['per_page']) ? intval($query['per_page']) : 20;
    $search = isset($query['search']) ? $query['search'] : null;
    $sort = isset($query['sort']) ? $query['sort'] != '' ? explode(",",$query['sort']) : [] : [];
    $filter = isset($query['filter']) ? $query['filter'] != '' ? explode(";",$query['filter']) : [] : [];
    $transaction = new Transaction();
    $offset = ($page - 1) * $per_page;
    $result = $transaction->getAll($per_page, $offset, $search, $sort, $filter, $query);

    $total = $result["total"];
    $transactions = $result["rows"];

    $meta = [
      "total" => $total,
      "page" => $page,
      "per_page" => $per_page,
      "offset" => $offset
    ];

    return array("transactions" => $transactions, "meta" => $meta);
  }

  public function getById($id) {
    $transaction = new Transaction();
    $result = $transaction->getById($id);
    return $result;
  }
}