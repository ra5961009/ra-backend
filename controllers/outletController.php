<?php
namespace Controller;
require_once("models/outletModel.php");

use Models\Outlet;

class OutletController {
  public function getByHotelsId($id) {
    try {
      $outlet = new Outlet();
      $result = $outlet->getByHotelsId($id);
      return $result;
    } catch (\Exception $e) {
      throw $e;
    }
  }
}