<?php
namespace Controller;
require_once("models/redeemModel.php");
require_once("models/memberModel.php");
require_once("models/voucherModel.php");
require_once("models/pointModel.php");

use Models\Redeem;
use Models\Voucher;
use Models\Member;
use Models\Point;

use \DateTime;

class RedeemController {
  public function create($body)  {
    try {
      $voucher = new Voucher();
      $redeem = new Redeem();
      $member = new Member();
      $point = new Point();

      $data_voucher = $voucher->getById($body->voucher_id);
      $data_member = $member->getById($body->member_id);

      if (!isset($data_voucher) || !isset($data_member) || sizeof($data_voucher) < 1 || sizeof($data_member) < 1) {
        http_response_code(400);
        echo json_encode(array("message" => "Bad request error."));
        return;
      }

      // Add code
      $body->member_code = $data_member['code'];
      $body->voucher_code = $data_voucher['code'];

      $member_point = $data_member['total_point_balance'];
      $voucher_point = $data_voucher['points'];
      $voucher_expires = $data_voucher['expire_date'];
      $voucher_qty = $data_voucher['qty'];

      $now = (new DateTime('now'))->format('Y-m-d');
      $expire = (new DateTime($voucher_expires))->format('Y-m-d');

      if ($expire < $now) {
        throw new \Exception("400;Voucher has been expired.");
      }
      
      if ($voucher_qty < 1) {
        throw new \Exception("400;Quota has run out.");
      }

      if ($member_point < $voucher_point) {
        throw new \Exception("400;Your total points not enough.");
      }

      $memberIdVoucherId = $redeem->getByMemberIdAndVoucherId($body->member_id, $body->voucher_id);
      if ($memberIdVoucherId >= 1) {
        throw new \Exception("400;1 Member can only get 1 voucher.");
      }
      
      $result = $redeem->create($body);

      $point_data = (object) array();
      $point_data->member_id = $data_member['id'];
      $point_data->phone_number = $data_member['phone_number'];
      $point_data->point = $data_voucher['points'];
      $point_data->voucher_id = $body->voucher_id;

      $data_point = $point->create($point_data);

      http_response_code(201);
      echo json_encode(array("voucher" => $result));
      return;
    } catch (\Exception $e) {
      throw $e;
    }
  }
  
  public function getAll($query) {
    $page = isset($query['page']) ? intval($query['page']) : 1;
    $per_page = isset($query['per_page']) ? intval($query['per_page']) : 20;
    $search = isset($query['search']) ? $query['search'] : null;
    $sort = isset($query['sort']) ? explode(":",$query['sort']) : [];

    $redeem = new Redeem();
    $offset = ($page - 1) * $per_page;
    $result = $redeem->getAll($per_page, $offset, $search, $sort);
    
    $total = $result["total"];
    $redeem = $result["rows"];
    
    $meta = [
      "total" => $total,
      "page" => $page,
      "per_page" => $per_page,
      "offset" => $offset
    ];

    $response = array("redeem" => $redeem, "meta" => $meta);
    http_response_code(200);
    echo json_encode($response);
    return;
  }

  public function getByMemberId($id) {
    $redeem = new Redeem();
    $result = $redeem->getByMemberId($id);
    return $result;
  }

  public function redeem($body, $param)  {
    try {
      $redeem = new Redeem();
      $member = new Member();
      $result = $redeem->redeem($param);
      $data = $redeem->getById($param);
      $memberData = $member->getById($data['member_id']);

      if(isset($memberData['fcm_token'])) {
        $params = array(
          'type' => 'token',
          'topic' => '',
          'token' => $memberData['fcm_token'],
          'title' => "Voucher Redeemed",
          'body' => $memberData['title'] . "." . $memberData['first_name'] . " " . $memberData['title'] . ", has redeemed the voucher with code" . $data['voucher_code']
        );
  
        $sendNotif = sendNotif($params);
        if ($sendNotif) {
          $notification = new Notification();
          $notifParams = new stdClass();
          $notifParams->phone_number = "";
          $notifParams->type = "Promotion";
          $notifParams->title = $body->title;
          $notifParams->body = $body->description;
          $notifParams->is_read = 0;
          $notifParams->platform = "mobile";
          
          $notification->create($notifParams);
        }
      }

      return $result;
    } catch (\Exception $e) {
      throw $e;
    }
  }
}