<?php
namespace Controller;
require_once("models/userModel.php");

use Models\User;

class UserController {
  public function register($body)  {
    if (!isset($body->email) || !isset($body->password) || !isset($body->role)) {
      throw new \Exception("400;Mandatory parameter[s].");
    }
    $user = new User();
    $hash = password_hash(strval($body->password), PASSWORD_DEFAULT);
    $body->password = $hash;
    $result = $user->create($body);
    return $result;
  }

  public function getAll($query) {
    $page = isset($query['page']) ? intval($query['page']) : 1;
    $per_page = isset($query['per_page']) ? intval($query['per_page']) : 20;
    $search = isset($query['search']) ? $query['search'] : null;
    $sort = isset($query['sort']) ? explode(":",$query['sort']) : [];

    $user = new User();
    $offset = ($page - 1) * $per_page;
    $result = $user->getAll($per_page, $offset, $search, $sort);
    
    $total = $result["total"];
    $users = $result["rows"];
    
    $meta = [
      "total" => $total,
      "page" => $page,
      "per_page" => $per_page,
      "offset" => $offset
    ];

    return array("users" => $users, "meta" => $meta);
  }

  public function edit($body, $param)  {
    try {
      $user = new User();
      $oldData = $user->getById($param);
      
      if ($oldData) {
        $body->name = $body->name ?? $oldData['name'];
        $body->role = $body->role ?? $oldData['role'];
  
        $user->edit($body, $param);
  
        http_response_code(200);
        echo json_encode(array("user" => $body));
      } else {
        http_response_code(400);
        echo json_encode(array("message" => "Bad request error."));
      }
      return;
    } catch (\Exception $e) {
      throw $e;
    }
  }

  public function getById($id) {
    $user = new User();
    $result = $user->getById($id);
    return $result;
  }

  public function _delete($param)  {
    try {
      $user = new User();
      $user->_delete($param);

      $result = ["id" => $param];
      return $result;
    } catch (\Exception $e) {
      throw $e;
    }
  }

}