<?php
namespace Controller;
require_once("models/cardModel.php");

use Models\Card;

class CardController {
 
  public function create($body)  {
    try {
      $card = new Card();
      $result = $card->create($body);
      return $body;
    } catch (\Exception $e) {
      throw $e;
    }
  }

  public function edit($body, $param)  {
    try {
      $card = new Card();
      $oldData = $card->getById($param);
      
      $body->name = $body->name ?? $oldData['name'];
      $body->image_url = $body->image_url ?? $oldData['image_url'];
      $body->min_points = $body->min_points ?? $oldData['min_points'];
      $body->max_points = $body->max_points ?? $oldData['max_points'];
      $body->benefit = $body->benefit ?? $oldData['benefit'];
      $body->qrcode_url = $body->qrcode_url ?? $oldData['qrcode_url'];

      $result = $card->edit($body, $param);
      return $result;
    } catch (\Exception $e) {
      throw $e;
    }
  }

  public function _delete($param)  {
    try {
      $card = new Card();
      $card->_delete($param);

      $result = ["id" => $param];
      return $result;
    } catch (\Exception $e) {
      throw $e;
    }
  }

  public function getAll() {
    $card = new Card();
    $result = $card->getAll();

    http_response_code(200);
    echo json_encode(array("card" => $result));

    return;
  }
  
  public function getById($id) {
    $card = new Card();
    $result = $card->getById($id);
    return $result;
  }

}