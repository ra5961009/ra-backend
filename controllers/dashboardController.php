<?php
namespace Controller;
require_once("models/dashboardModel.php");

use Models\Dashboard;

class DashboardController {
  public function summary()  {
    try {
      $dashboard = new Dashboard();
      $result = $dashboard->summary();
      http_response_code(200);
      echo json_encode(array(
        "summary" => $result
      ));
        
      return;
    } catch (\Exception $e) {
      throw $e;
    }
  }
}