<?php
namespace Controller;
require_once("models/promotionsModel.php");
require_once("models/notificationModel.php");
require_once("utils/notif.php");

use Models\Promotions;
use Models\Notification;
use stdClass;

class PromotionsController {
  public function create($body) {
    try {
      $promotions = new Promotions();
      $result = $promotions->create($body);

      $params = array(
        'type' => 'topic',
        'topic' => 'staging_promotion', // staging_promotion for Staging and production_promotion for Production
        'token' => null,
        'title' => $body->name,
        'body' => $body->description
      );

      $sendNotif = sendNotif($params);
      if ($sendNotif) {
        $notification = new Notification();
        $notifParams = new stdClass();
        $notifParams->phone_number = "";
        $notifParams->type = "Promotion";
        $notifParams->title = $body->name;
        $notifParams->body = $body->description;
        $notifParams->is_read = 0;
        $notifParams->platform = "mobile";
        
        $notification->create($notifParams);
      }

      http_response_code(201);
      echo json_encode(array("promotions" => $body));
      return;
    } catch (\Exception $e) {
      throw $e;
    }
  }

  public function edit($body, $param)  {
    try {
      $promotions = new Promotions();
      $oldData = $promotions->getById($param);
      
      $body->hotels_id = $body->hotels_id ?? $oldData['hotels_id'];
      $body->name = $body->name ?? $oldData['name'];
      $body->description = $body->description ?? $oldData['description']; 
      $body->img_url = $body->img_url ?? $oldData['img_url']; 
      
      $result = $promotions->edit($body, $param);

      http_response_code(200);
      echo json_encode(array("promotions" => $body));
      return $result;
    } catch (\Exception $e) {
      throw $e;
    }
  }

  public function _delete($param)  {
    try {
      $promotions = new Promotions();
      $promotions->_delete($param);

      $result = ["id" => $param];
      return $result;
    } catch (\Exception $e) {
      throw $e;
    }
  }

  public function getAll() {
    $promotions = new Promotions();
    $result = $promotions->getAll();
    return $result;
  }
  
  public function getByHotelsId($id) {
    $promotions = new Promotions();
    $result = $promotions->getByHotelsId($id);
    return $result;
  }
  
}