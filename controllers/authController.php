<?php
namespace Controller;
require_once("models/userModel.php");
require_once("models/memberModel.php");

use Models\User;
use Models\Member;

class UserController {
  private function getToken($payload) {
    $header = base64_encode(json_encode(["alg" => "HS256", "typ" => "JWT"]));
    $payload_json = base64_encode(json_encode($payload));
    
    $signature = hash_hmac('sha256', "$header.$payload_json", JWT_SECRET, true);
    $signature_base64 = base64_encode($signature);
    
    $jwt_token = "$header.$payload_json.$signature_base64";
    
    return $jwt_token;
  }

  public function login($body) {
    if (!isset($body->username) || !isset($body->password) || !isset($body->platform)) {
      throw new \Exception("400;Mandatory parameter[s].");
    }
    
    if(!in_array($body->platform, array('web','mobile'))) {
      throw new \Exception("400;Mandatory parameter[s].");
    }

    $result = null;
    $payload = null;
    if ($body->platform != 'web') {
      $member = new Member();
      if (filter_var($body->username, FILTER_VALIDATE_EMAIL)) {
        $result = $member->getByEmail($body->username);
      } else {
        $result = $member->getByPhone($body->username);
      }
      
      if (isset($result)) {
        if (filter_var($body->username, FILTER_VALIDATE_EMAIL)) {
          $member->updateFcmTokenByEmail($body->username, $body->fcmToken);
        } else {
          $member->updateFcmTokenByPhone($body->username, $body->fcmToken);
        }
        
        $payload = [
          "id" => $result['id'],
          "first_name" => $result['first_name'],
          "last_name" => $result['last_name'],
          "email" => $result['email'],
          "platform" => $body->platform,
          "exp" => time() + (600 * 4800) // Token expiry time (8 hour from now)
        ];
      }
    } else {
      $user = new User();
      $result = $user->getByEmail($body->username);
      if (isset($result)) {
        $payload = [
          "id" => $result['id'],
          "name" => $result['name'],
          "email" => $result['email'],
          "role" => $result['role'],
          "platform" => $body->platform,
          "exp" => time() + (600 * 4800) // Token expiry time (1 hour from now)
        ];
      }
    }

    $password = isset($result) ? $result['password'] : '';
    $password_validate = password_verify($body->password, $password);
    
    if ($password_validate) {
      $token = $this->getToken($payload);
      $result["access_token"] = $token;
      unset($result["password"]);
      
      return $result;
    } else {
      throw new \Exception("400;Check username or password.");
    }
  }

  public function forgot($body) {
    if (!isset($body->email)) {
      throw new \Exception("400;Mandatory parameter[s].");
    }
    
    if(!in_array($body->platform, array('web','mobile'))) {
      throw new \Exception("400;Mandatory parameter[s].");
    }

    if ($body->platform != 'web') {
      $member = new Member();
      if (filter_var($body->email, FILTER_VALIDATE_EMAIL)) {
        $result = $member->getByEmail($body->email);
      } else {
        $result = $member->getByPhone($body->email);
      }
      
      if (isset($result)) {
        $payload = [
          "id" => $result['id'],
          "first_name" => $result['first_name'],
          "last_name" => $result['last_name'],
          "email" => $result['email'],
          "platform" => $body->platform,
          "exp" => time() + (60 * 480) // Token expiry time (8 hour from now)
        ];
        return $this->sendActivationEmail($result['email'], ($result['first_name']. ' '.$result['last_name']));
      } else {
        throw new \Exception("400;Email not found.");
      }
    } else {
      $user = new User();
      $result = $user->getByEmail($body->email);
      if (isset($result)) {
        $payload = [
          "id" => $result['id'],
          "name" => $result['name'],
          "email" => $result['email'],
          "role" => $result['role'],
          "platform" => $body->platform,
          "exp" => time() + (60 * 480) // Token expiry time (1 hour from now)
        ];
        return $this->sendActivationEmail($result['email'], $result['name']);
      } else {
        throw new \Exception("400;Email not found.");
      }
    }
  }

  private function sendActivationEmail($email, $name) {
    // Contoh penggunaan
      $to = $email;
      $subject = "Permintaan Atur Ulang Kata Sandi Anda";
      $body = "
      Halo ".$name.",
      <br /><br />
      Kami menerima permintaan untuk mengatur ulang kata sandi akun Anda. Jika Anda tidak melakukan permintaan ini, Anda dapat mengabaikan email ini dan kata sandi Anda akan tetap sama.
      <br /><br /> 
      Untuk mengatur ulang kata sandi Anda, silakan klik tautan di bawah ini:
      <br /><br />
      <a href='https://ra.fivto.id/forgot.php'>Forgot Password</a>
      <br /><br />
      Tautan ini akan kedaluwarsa dalam 24 jam untuk keamanan akun Anda. Jika tautan sudah kedaluwarsa, Anda dapat mengajukan permintaan atur ulang kata sandi baru.
      <br /><br />
      Jika Anda membutuhkan bantuan lebih lanjut, jangan ragu untuk menghubungi tim dukungan kami.
      <br /><br />
      Terima kasih,<br />
      Tim Dukungan Aurora Ra
      <br /><br />
      Selamat Mencoba, Salam Sukes !
      ";
      return $this->sendEmail($to, $subject, $body);
  }

  private function sendEmail($to, $subject, $body) {
    $from = "no-reply@loyalty.fivto.id";
    $headers = "MIME-Version: 1.0" . "\r\n";
    $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
    $headers .= "From: <$from>" . "\r\n";

    if (mail($to, $subject, $body, $headers)) {
        return "Email berhasil dikirim ke $to";
    } else {
        return "Gagal mengirim email ke $to";
    }
  }

  public function logout($body) {
    if (!isset($body->email)) {
      throw new \Exception("400;Mandatory parameter[s].");
    }
    
    $member = new Member();
    if (filter_var($body->email, FILTER_VALIDATE_EMAIL)) {
      $result = $member->getByEmail($body->email);
    } else {
      $result = $member->getByPhone($body->email);
    }
    
    if (isset($result)) {
      $member->updateFcmTokenByEmail($body->email, '');
    } else {
      throw new \Exception("400;Email not found.");
    }
    
  }

}