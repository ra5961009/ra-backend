<?php
namespace Controller;
require_once("models/notificationModel.php");

use Models\Notification;

class NotificationController {
  public function getAll($query) {
    $page = isset($query['page']) ? intval($query['page']) : 1;
    $per_page = isset($query['per_page']) ? intval($query['per_page']) : 20;
    $search = isset($query['search']) ? $query['search'] : null;
    $sort = isset($query['sort']) ? $query['sort'] != '' ? explode(",",$query['sort']) : [] : [];
    $filter = isset($query['filter']) ? $query['filter'] != '' ? explode(";",$query['filter']) : [] : [];
    $notification = new Notification();
    $offset = ($page - 1) * $per_page;
    $result = $notification->getAll($per_page, $offset, $search, $sort, $filter);

    $total = $result["total"];
    $notifications = $result["rows"];

    $meta = [
      "total" => $total,
      "page" => $page,
      "per_page" => $per_page,
      "offset" => $offset
    ];

    $response = array("notifications" => $notifications, "meta" => $meta);
    http_response_code(200);
    echo json_encode($response);
  }
}