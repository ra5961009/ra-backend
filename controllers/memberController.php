<?php
namespace Controller;
require_once("models/memberModel.php");

use Models\Member;
use stdClass;

class MemberController {
  public function create($body)  {
    try {
      $member = new Member();
      $hash = password_hash(strval(isset($body->password) ? $body->password : '1234'), PASSWORD_DEFAULT);
      $body->password = $hash;

      $check = $member->checkEmailPhone($body->email, $body->phone_number);
      if ($check) {
        http_response_code(400);
        echo json_encode(array("data" => $check, "message" => "Email or phone number already exist."));
        return;
      }

      $result = $member->create($body);
      
      $params = new stdClass();
      $params->member_id = ""; 
      $params->phone_number = $body->phone_number; 
      $params->operation = "in"; 
      $params->note = "register"; 

      $resultPoints = $member->createPoints($params);
      http_response_code(201);
      echo json_encode(array("member" => $body, "message" => "Success."));

      return;
    } catch (\Exception $e) {
      throw $e;
    }
  }

  public function edit($body, $param)  {
    try {
      $member = new Member();
      $oldData = $member->getById($param);
      
      $body->first_name = $body->first_name ?? $oldData['first_name'];
      $body->last_name = $body->last_name ?? $oldData['last_name'];
      $body->code = $body->code ?? $oldData['code'];
      $body->email = $body->email ?? $oldData['email'];
      $body->phone_number = $body->phone_number ?? $oldData['phone_number'];
      $body->total_point_balance = $body->total_point_balance ?? $oldData['total_point_balance'];
      $body->img_url = $body->img_url ?? $oldData['img_url'];
      $body->register_date = $body->register_date ?? $oldData['register_date'];
      $body->tier = $body->tier ?? $oldData['tier'];
      //$body->referral_code = $body->referral_code ?? $oldData['referral_code'];
      
      $result = $member->edit($body, $param);
      return $result;
    } catch (\Exception $e) {
      throw $e;
    }
  }

  public function _delete($param)  {
    try {
      $member = new Member();
      $member->_delete($param);

      $result = ["id" => $param];
      return $result;
    } catch (\Exception $e) {
      throw $e;
    }
  }

  public function getAll($query) {
    $page = isset($query['page']) ? intval($query['page']) : 1;
    $per_page = isset($query['per_page']) ? intval($query['per_page']) : 20;
    $search = isset($query['search']) ? $query['search'] : null;
    $sort = isset($query['sort']) ? $query['sort'] != '' ? explode(",",$query['sort']) : [] : [];
    $filter = isset($query['filter']) ? $query['filter'] != '' ? explode(";",$query['filter']) : [] : [];
    $member = new Member();
    $offset = ($page - 1) * $per_page;
    $result = $member->getAll($per_page, $offset, $search, $sort, $filter, $query);

    $total = $result["total"];
    $members = $result["rows"];

    $meta = [
      "total" => $total,
      "page" => $page,
      "per_page" => $per_page,
      "offset" => $offset
    ];

    return array("members" => $members, "meta" => $meta);
  }

  public function getById($id) {
    $member = new Member();
    $result = $member->getById($id);
    return $result;
  }

  public function update($body)  {
    $member = new Member();
    $result = $member->update($body);
    return $result;
  }

  public function updateActive($body)  {
    $member = new Member();
    $result = $member->updateActive($body);
    return $result;
  }

  public function set_starting_point($body) {
    $member = new Member();
    $result = $member->set_starting_point($body);
    if ($result) {
      http_response_code(200);
      echo json_encode(array("data" => $body->value));
    }
    return $result;
  }

  public function get_starting_point() {
    $member = new Member();
    $result = $member->get_starting_point();
    if ($result) {
      http_response_code(200);
      echo json_encode(array("data" => $result));
    }
    return $result;
  }
}