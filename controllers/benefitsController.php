<?php
namespace Controller;
require_once("models/benefitsModel.php");
require_once("models/notificationModel.php");
require_once("utils/notif.php");

use Models\benefits;
use Models\Notification;
use stdClass;

class BenefitsController {

   public function create($body)  {
    try {
      $benefits = new Benefits();
      $benefits->create($body);

      http_response_code(201);
      echo json_encode(array("message" => "Congratulations, the benefits have been successfully saved."));

      return;
    } catch (\Exception $e) {
      throw $e;
    }
  }

  public function edit($body, $param)  {
    try {
      $benefits = new Benefits();
      $oldData = $benefits->getById($param);
      
      $body->hotels_id = $body->hotels_id ?? $oldData['hotels_id'];
      $body->outlet_code = $body->outlet_code ?? $oldData['outlet_code'];
      $body->type = $body->type ?? $oldData['type'];
      $body->article_number = $body->article_number ?? $oldData['article_number'];
      $body->title = $body->title ?? $oldData['title'];
      $body->description = $body->description ?? $oldData['description'];
      $body->plu = $body->plu ?? $oldData['plu'];
      $body->discount = $body->discount ?? $oldData['discount'];
      $body->price = $body->price ?? $oldData['price'];
      $body->category = $body->category ?? $oldData['category'];
      $body->code = $body->code ?? $oldData['code'];
      $body->qty = $body->qty ?? $oldData['qty'];
      $body->expire_date = $body->expire_date ?? $oldData['expire_date'];
      $body->image_url = $body->image_url ?? $oldData['image_url'];

      $result = $benefits->edit($body, $param);
      return $result;
    } catch (\Exception $e) {
      throw $e;
    }
  }

  public function _delete($param)  {
    try {
      $benefits = new Benefits();
      $benefits->_delete($param);

      $result = ["id" => $param];
      return $result;
    } catch (\Exception $e) {
      throw $e;
    }
  }

  public function getByHotelId($id) {
    $benefits = new Benefits();
    $result = $benefits->getByHotelId($id);
    return $result;
  }

  public function getAll($query) {
    $page = isset($query['page']) ? intval($query['page']) : 1;
    $per_page = isset($query['per_page']) ? intval($query['per_page']) : 20;
    $search = isset($query['search']) ? $query['search'] : null;
    $sort = isset($query['sort']) ? explode(":",$query['sort']) : [];

    $benefit = new Benefits();
    $offset = ($page - 1) * $per_page;
    $result = $benefit->getAll($per_page, $offset, $search, $sort);
    
    $total = $result["total"];
    $benefits = $result["rows"];
    
    $meta = [
      "total" => $total,
      "page" => $page,
      "per_page" => $per_page,
      "offset" => $offset
    ];

    return array("benefits" => $benefits, "meta" => $meta);
  }
  

}