<?php
namespace Controller;
require_once("models/voucherModel.php");
require_once("models/notificationModel.php");
require_once("utils/notif.php");

use Models\voucher;
use Models\Notification;
use stdClass;

class VoucherController {

  public function create($body)  {
    try {
      $voucher = new Voucher();
      $result = $voucher->create($body);

      $params = array(
        'type' => 'topic',
        'topic' => 'staging_new_voucher', // staging_new_voucher for Staging and production_new_voucher for Production
        'token' => null,
        'title' => $body->title,
        'body' => $body->description
      );

      $sendNotif = sendNotif($params);
      if ($sendNotif) {
        $notification = new Notification();
        $notifParams = new stdClass();
        $notifParams->phone_number = "";
        $notifParams->type = "Promotion";
        $notifParams->title = $body->title;
        $notifParams->body = $body->description;
        $notifParams->is_read = 0;
        $notifParams->platform = "mobile";
        
        $notification->create($notifParams);
      }

      http_response_code(201);
      echo json_encode(array("voucher" =>  $body));

      return;
    } catch (\Exception $e) {
      throw $e;
    }
  }

  public function edit($body, $param)  {
    try {
      $voucher = new Voucher();
      $oldData = $voucher->getById($param);
      
      $body->title = $body->title ?? $oldData['title'];
      $body->description = $body->description ?? $oldData['description'];
      $body->term_condition = $body->term_condition ?? $oldData['term_condition'];
      $body->category = $body->category ?? $oldData['category'];
      $body->code = $body->code ?? $oldData['code'];
      $body->qty = $body->qty ?? $oldData['qty'];
      $body->expire_date = $body->expire_date ?? $oldData['expire_date'];
      $body->points = $body->points ?? $oldData['points'];
      $body->image_url = $body->image_url ?? $oldData['image_url'];

      $result = $voucher->edit($body, $param);
      return $result;
    } catch (\Exception $e) {
      throw $e;
    }
  }

  public function _delete($param)  {
    try {
      $voucher = new Voucher();
      $voucher->_delete($param);

      $result = ["id" => $param];
      return $result;
    } catch (\Exception $e) {
      throw $e;
    }
  }

  public function getAll($query) {
    $page = isset($query['page']) ? intval($query['page']) : 1;
    $per_page = isset($query['per_page']) ? intval($query['per_page']) : 20;
    $search = isset($query['search']) ? $query['search'] : null;
    $sort = isset($query['sort']) ? explode(":",$query['sort']) : [];

    $voucher = new Voucher();
    $offset = ($page - 1) * $per_page;
    $result = $voucher->getAll($per_page, $offset, $search, $sort);
    
    $total = $result["total"];
    $vouchers = $result["rows"];
    
    $meta = [
      "total" => $total,
      "page" => $page,
      "per_page" => $per_page,
      "offset" => $offset
    ];

    return array("vouchers" => $vouchers, "meta" => $meta);
  }

  public function getByHotelsId($id) {
    $voucher = new Voucher();
    $result = $voucher->getByHotelsId($id);
    return $result;
  }
  
  public function getById($id) {
    $voucher = new Voucher();
    $result = $voucher->getById($id);
    return $result;
  }

}