<?php
function loadEnv($file)
{
  if (!file_exists($file)) {
    throw new Exception("File $file tidak ditemukan");
  }

  $lines = file($file, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);

  foreach ($lines as $line) {
    if (strpos(trim($line), '#') === 0) {
        continue;
    }
    list($name, $value) = explode('=', $line, 2);
    $name = trim($name);
    $value = trim($value);
    $value = trim($value, "'\"");
    $_ENV[$name] = $value;
    $_SERVER[$name] = $value;
  }
}

function initializeEnv()
{
  loadEnv(__DIR__ . '/.env');
  $appEnv = $_ENV['APP_ENV'] ?? 'development';
  $envFile = __DIR__ . "/.env.$appEnv";
  loadEnv($envFile);
}
