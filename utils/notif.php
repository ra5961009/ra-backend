<?php
function sendNotif($params) {
  // Your Firebase Server Key
  $serverKey = $_ENV['FCM_SERVER_KEY'];

  $to = $params['token'];
  if ($params['type'] == 'topic') {
    $to = '/topics/' . $params['topic'];
  }

  // Set up fields for the notification payload
  $fields = array(
    'to' => $to,
    'notification' => array(
      'title' => $params['title'],
      'body' => $params['body'],
      'OrganizationId' => '2',
      'content_available' => true,
      'priority' => 'high',
      'subtitle' => 'Elementary School'
    ),
    'data' => array(
      'priority' => 'high',
      'sound' => 'app_sound.wav',
      'content_available' => true,
      'bodyText' => $params['body'],
      'organization' => 'Elementary school'
    )
  );

  // Set up headers for the HTTP request
  $headers = array(
      'Authorization: key=' . $serverKey,
      'Content-Type: application/json'
  );

  // Initialize cURL
  $ch = curl_init();

  // Configure cURL options
  curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
  curl_setopt($ch, CURLOPT_POST, true);
  curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
  curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

  // Execute the cURL request and get the response
  $result = curl_exec($ch);

  // Check for cURL errors
  if ($result === FALSE) {
      die('Curl failed: ' . curl_error($ch));
  }

  // Close cURL
  curl_close($ch);

  // Return the result
  return $result;
}